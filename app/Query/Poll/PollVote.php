<?php

namespace App\Query\Poll;

use Illuminate\Database\Eloquent\Model;

class PollVote extends Model
{
    protected $fillable = ['user_id', 'poll_id'];

    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }

    public function poll()
    {
    	return $this->belongsTo(\App\Query\Poll\Poll::class);
    }
}

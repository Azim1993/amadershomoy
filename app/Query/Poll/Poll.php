<?php

namespace App\Query\Poll;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Poll extends Model
{
    protected $fillable = ['user_id', 'group_post_id', 'feature', 'status'];

    public function store($groupPostId, $data)
    {
        return static::create([
            'user_id' => Auth::user()->id,
            'group_post_id' => $groupPostId,
            'feature' => $data,
        ]);
    }

    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }

    public function groupPost()
    {
    	return $this->belongsTo(\App\Query\Admin\Post\Group\GroupPost::class);
    }

    public function votes()
    {
        return $this->hasMany(\App\Query\Poll\PollVote::class);
    }

}

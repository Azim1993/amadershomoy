<?php

namespace App\Query\Poll;

use Illuminate\Database\Eloquent\Model;

class PollFeature extends Model
{
   	protected $fillable = ['poll_id', 'feature', 'status'];

   	public function poll()
   	{
   		return $this->belongsTo(\App\Query\Poll\Poll::class);
   	}

    public function votes()
    {
    	return $this->hasMany(\App\Query\Poll\PollVote::class);
    }
}

<?php

namespace App\Query\Post;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
	protected $fillable = ['user_id', 'blog_id', 'type', 'fileName'];

    public function post()
    {
    	return $this->belongsTo(\App\Query\Post\Blog::class);
    }
    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }
    
}

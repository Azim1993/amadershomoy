<?php

namespace App\Query\Post;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
	protected $fillable = ['user_id', 'blog_id', 'status'];
	
    public function post()
    {
    	return $this->belongsTo(\App\Query\Post\Blog::class);
    }
    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\user::class);
    }
}

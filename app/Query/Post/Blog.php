<?php

namespace App\Query\Post;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Blog extends Model
{
    protected $fillable = ['user_id', 'blog_text', 'status'];

    public function store($data)
    {
        return static::create([
                'user_id'   => Auth::user()->id,
                'blog_text' => $data,
            ]);
    }

    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }
    public function likes()
    {
    	return $this->hasMany(\App\Query\Post\Like::class);
    }
    public function comments()
    {
        return $this->hasMany(\App\Query\Post\Comment::class);
    }
    public function shares()
    {
        return $this->hasMany(\App\Query\Post\Share::class);
    }
    public function totalLike()
    {
    	return $this->like()->count();
    }
    public function assets()
    {
    	return $this->hasMany(\App\Query\Post\Asset::class);
    }
}

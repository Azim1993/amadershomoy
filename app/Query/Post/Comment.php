<?php

namespace App\Query\Post;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id', 'blog_id', 'comment','parent_id','status'];
    public function post()
    {
    	return $this->belongsTo(\App\Query\Post\Blog::class);
    }

    public function scopeParent(Builder $query)
    {
    	return $query->where('parent_id',0);
    }

    public function scopeChild(Builder $query, $parent)
    {
    	return $query->where('parent_id',$parent);
    }

    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }
}

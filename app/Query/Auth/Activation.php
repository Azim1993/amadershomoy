<?php

namespace App\Query\Auth;

use Illuminate\Database\Eloquent\Model;

class Activation extends Model
{
    protected $fillable = ['token'];

    public function incrementStatus()
    {
    	return $this->status->increment();
    }

    public function getTokenByUserId($userId)
    {
    	return self::where('user_id', $userId);
    }

    public static function getUserByToken($token)
    {
    	return self::where('token', $token)->first();
    }

    public function activeTokenUser($token)
    {
    	return self::getUserByToken($token)->select('status') == 1;
    }

    public function checkToken($token)
    {
    	return $this->getUserByToken($token) ? true : false;
    }

    
}

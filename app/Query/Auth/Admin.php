<?php

namespace App\Query\Auth;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

	protected $guard = 'admin';
	protected $fillable = ['email', 'password'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function infoMessage()
    {
        return $this->hasMany(\App\Query\Admin\User\InfoMessage::class, 'admin_id');
    }

    public function certificates()
    {
    	return $this->hasMany(\App\Query\Admin\User\Certificate::class);
    }

    public function polls()
    {
        return $this->hasMany(\ App\Query\Admin\Poll\AdminPoll::class);
    }
}

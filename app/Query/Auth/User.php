<?php

namespace App\Query\Auth;

use App\Query\User\UserPostTrait;
use App\Query\User\userTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable, userTrait, UserPostTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'user_name', 'password', 'phone'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','login_status','status'
    ];

    public function blockStore()
    {
        $this->login_status = true;
        return $this->save();
    }

    public static function blockUser()
    {
        return self::where('login_status',true);
    }

    public function warning()
    {
        $this->status = 1;
        return $this->save();
    }

    public function finalWarning()
    {
        $this->status = 2;
        return $this->save();
    }
}

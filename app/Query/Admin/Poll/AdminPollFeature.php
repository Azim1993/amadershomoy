<?php

namespace App\Query\Admin\Poll;

use Illuminate\Database\Eloquent\Model;

class AdminPollFeature extends Model
{
    protected $fillable = ['admin_poll_id', 'feature', 'status'];

    public function adminPoll()
    {
    	return $this->belongsTo(\App\Query\Admin\Poll\AdminPoll::class);
    }

    public function adminPollVotes()
    {
    	return $this->hasMany(\App\Query\Admin\Poll\AdminPollVote::class);
    }
}

<?php

namespace App\Query\Admin\Poll;

use Illuminate\Database\Eloquent\Model;

class AdminPollVote extends Model
{
    protected $fillable = ['user_id', 'admin_poll_feature_id'];

    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }
    public function adminFeaturePoll()
    {
    	return $this->belongsTo(\App\Query\Admin\Poll\AdminPollFeature::class);
    }
}

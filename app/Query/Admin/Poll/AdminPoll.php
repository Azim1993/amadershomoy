<?php

namespace App\Query\Admin\Poll;

use Illuminate\Database\Eloquent\Model;

class AdminPoll extends Model
{
    protected $fillable = ['admin_id', 'title', 'summary', 'cover', 'status'];

    public function features()
    {
    	return $this->hasMany(\App\Query\Admin\Poll\AdminPollFeature::class);
    }
}

<?php

namespace App\Query\Admin\User;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    protected $fillable = ['title', 'detail', 'cover', 'status'];

    public function admin()
    {
    	return $this->belongsTo(\App\Query\Auth\Admin::class);
    }

    public function certificateUsers()
    {
    	return $this->hasMany(\App\Query\Admin\User\CertificateUser::class);
    }
}

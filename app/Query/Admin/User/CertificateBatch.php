<?php

namespace App\Query\Admin\User;

use Illuminate\Database\Eloquent\Model;

class CertificateBatch extends Model
{
    protected $fillable = ['batch'];

    public function users()
    {
    	return $this->hasMany(\App\Query\Admin\User\CertificateUser::class);
    }
}

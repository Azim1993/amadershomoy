<?php

namespace App\Query\Admin\User;

use Illuminate\Database\Eloquent\Model;

class CertificateUser extends Model
{
    protected $fillable = ['user_id', 'certificate_id', 'certificate_batch_id', 'taken', 'status'];

    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }

    public function certificate()
    {
    	return $this->belongsTo(\App\Query\Admin\User\Certificate::class);
    }

    public function certificateBatch()
    {
    	return $this->belongsTo(\App\Query\Admin\User\CertificateBatch::class);
    }
}

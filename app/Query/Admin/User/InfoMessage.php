<?php

namespace App\Query\Admin\User;

use Illuminate\Database\Eloquent\Model;

class InfoMessage extends Model
{
    protected $fillable = ['user_id', 'message', 'status'];

    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }
}

<?php

namespace App\Query\Admin\Training;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $fillable = ['name','logo'];

    public function users()
    {
    	return $this->belongsToMany(\App\Query\Auth\User::class, 'training_user');
    }    
}

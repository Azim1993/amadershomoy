<?php

namespace App\Query\Admin\Training;

use Illuminate\Database\Eloquent\Model;

class TrainingUser extends Model
{
	protected $table = 'training_user';

    protected $fillable = ['user_id', 'training_id'];

    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }
}

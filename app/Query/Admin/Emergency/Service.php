<?php

namespace App\Query\Admin\Emergency;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['name'];

    public function contacts()
    {
    	return $this->hasMany(\App\Query\Admin\Emergency\EmergencyContact::class);
    }
}

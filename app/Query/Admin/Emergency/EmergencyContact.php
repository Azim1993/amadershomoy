<?php

namespace App\Query\Admin\Emergency;

use Illuminate\Database\Eloquent\Model;

class EmergencyContact extends Model
{
    protected $fillable = ['service_id','district','upazila','location','contact_number'];

    public function service()
    {
    	return $this->belongsTo(\App\Query\Admin\Emergency\Service::class);
    }

    public function districtName()
    {
    	return $this->belongsTo(\App\Query\Location\District::class,'district')->select('id','name');
    }

    public function upazilaName()
    {
    	return $this->belongsTo(\App\Query\Location\Upazila::class,'upazila')->select('id','name');
    }
}

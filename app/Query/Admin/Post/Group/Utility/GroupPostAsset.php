<?php

namespace App\Query\Admin\Post\Group\Utility;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GroupPostAsset extends Model
{
	protected $fillable = ['user_id', 'group_post_id', 'type', 'fileName'];

	public function store($postId, $fileName, $ext)
	{
		return static::create([
			'user_id' => Auth::user()->id,
			'group_post_id' => $postId,
			'type' => $this->fileType($ext),
			'fileName' => $fileName
		]);
	}

    private function fileType($ext)
    {
        return in_array($ext, ['png','jpg','jpeg','svg','bmp','gif'])?1:2;
    }

    public function post()
    {
    	return $this->belongsTo(\App\Query\Admin\Post\Group\GroupPost::class);
    }
    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }
}

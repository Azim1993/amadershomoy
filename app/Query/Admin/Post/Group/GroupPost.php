<?php

namespace App\Query\Admin\Post\Group;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class GroupPost extends Model
{
    protected $fillable = ['user_id', 'blog_text', 'group_id', 'status'];

    public function storePost($group, $data)
    {
        return static::create([
            'user_id' => Auth::user()->id,
            'blog_text' => $data,
            'group_id' => $group
        ]);
    }
    public function user()
    {
        return $this->belongsTo(\App\Query\Auth\User::class);
    }
    public function group()
    {
        return $this->belongsTo(\App\Query\Admin\Post\Group\Group::class);
    }

    public function polls()
    {
        return $this->hasMany(\App\Query\Poll\Poll::class);
    }

    public function assets()
    {
        return $this->hasMany(\App\Query\Admin\Post\Group\Utility\GroupPostAsset::class);
    }
}

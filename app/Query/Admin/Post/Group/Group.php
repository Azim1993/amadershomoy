<?php

namespace App\Query\Admin\Post\Group;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name', 'detail', 'cover', 'logo', 'group_parent_id'];


    public function store($data)
    {
        return static::create($data);
    }

    public function scopeParent($query)
    {
    	return $query->where('group_parent_id',0);
    }

    public function child()
    {
        return $this->hasMany(\App\Query\Admin\Post\Group\Group::class,'group_parent_id');
    }

    public function parentTo()
    {
        return $this->belongsTo(\App\Query\Admin\Post\Group\Group::class,'group_parent_id')->select('id','name');
    }

    public function scopeChildGroup($query)
    {
    	return $query->where('group_parent_id','>', 0);
    }

    public function groupUsers()
    {
    	return $this->hasMany(\App\Query\Admin\Post\Group\GroupUser::class);
    }

    public function posts()
    {
        return $this->hasMany(\App\Query\Admin\Post\Group\GroupPost::class);
    }
}

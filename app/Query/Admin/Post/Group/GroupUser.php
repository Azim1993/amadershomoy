<?php

namespace App\Query\Admin\Post\Group;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
    protected $fillable = ['user_id', 'group_id', 'status'];

    public function user()
    {
    	return $this->belongsTo(\App\Query\Auth\User::class);
    }

    public function group()
    {
    	return $this->belongsTo(\App\Query\Admin\Post\Group\Group::class);
    }

    public function activeUser()
    {
    	return $this->status = true;
    }

    public function pendingUser()
    {
    	return $this->status = false;
    }
}

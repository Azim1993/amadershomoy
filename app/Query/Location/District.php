<?php

namespace App\Query\Location;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public function upazilas()
    {
    	return $this->hasMany(\App\Query\Location\Upazila::class, 'district_id');
    }
}

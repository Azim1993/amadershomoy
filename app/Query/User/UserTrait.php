<?php namespace App\Query\User;

trait userTrait {
	public function register($data)
	{
		return self::create([
            'email' => $data['email'],
            'user_name' => $data['user_name'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
		]);
	}

	public function follow()
	{
		return $this->hasMany(\App\Query\User\Profile\Follow::class);
	}

	public function follower()
	{
		return $this->follow()->allFollower();
	}

	public function followCount() {
		return $this->follower()->count();
	}

	public function following()
	{
		return $this->follow()->allFollowing()->count();
	}

	public function basicInfo()
	{
		return $this->hasOne(\App\Query\User\Profile\BasicInfo::class);
	}

	public function fullName()
	{
		return $this->basicInfo()->select('user_id', 'first_name','last_name');
	}

	public function postUser()
	{
		return $this->basicInfo()->select('first_name','last_name')
			->select('user_id','first_name','last_name');
	}

	public function awards()
	{
		return $this->hasMany(\App\Query\User\Profile\Award::class);
	}
	public function contact()
	{
		return $this->hasMany(\App\Query\User\Profile\Contact::class);
	}
	public function educations()
	{
		return $this->hasMany(\App\Query\User\Profile\Education::class);
	}
	public function works()
	{
		return $this->hasMany(\App\Query\User\Profile\Work::class);
	}
	public function profileImages()
	{
		return $this->hasMany(\App\Query\User\Profile\ProfileImage::class);
	}

	public function forCurrentProfile()
	{
		return $this->hasOne(\App\Query\User\Profile\ProfileImage::class);
	}

	public function currentProfile()
	{
		return $this->forCurrentProfile()->avatar()->select('user_id','image');
	}

	public function currentCover()
	{
		return $this->forCurrentProfile()->cover()->select('user_id','image');
	}

	public function post()
	{
		return $this->hasMany(\App\Query\Post\Blog::class);
	}

	public function postAsset()
	{
		return $this->hasMany(\App\Query\Post\Asset::class);
	}

	public function likes()
	{
		return $this->hasMany(\App\Query\Post\Like::class);
	}

	public function comments()
	{
		return $this->hasMany(\App\Query\Post\Comment::class);
	}

	public function shares()
	{
		return $this->hasMany(\App\Query\Post\Share::class);
	}

	public function infoMessages()
	{
		return $this->hasMany(\App\Query\Admin\User\InfoMessage::class);
	}

	public function certificates()
	{
		return $this->hasMany(\App\Query\Admin\User\CertificateUser::class);
	}

	public function groups()
	{
		return $this->hasMany(\App\Query\Admin\Post\Group\GroupUser::class);
	}

	public function groupPosts()
	{
		return $this->hasMany(\App\Query\Admin\Post\Group\GroupPost::class);
	}

	public function adminPollVotes()
	{
		return $this->hasMany(\App\Query\Admin\Poll\AdminPollVote::class);
	}

	public function groupPolls()
	{
		return $this->hasMany(\App\Query\Poll\Poll::class);
	}

	public function groupPollsVotes()
	{
		return $this->hasMany(\App\Query\Poll\PollVote::class);
	}
}
<?php

namespace App\Query\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Rating extends Model
{
    protected $fillable = ['user_id', 'rate_user_id', 'point', 'detail'];

    public function user()
    {
    	return $this->hasOne(\App\Query\Auth\User::class);
    }
    public function rater()
    {
    	return $this->hasOne(\App\Query\Auth\User::class, 'rater_id');
    }
    public function store($rateTo, $data)
    {
    	return static::create([
    		'rate_user_id'=> Auth::user()->id,
    		'user_id' => $rateTo,
    		'point'   => $data
    	]);
    }

    public function scopeCheckRate($query, $user)
    {
        return $query->where([
            'user_id' => Auth::user()->id,
            'rate_user_id' => $user
        ]);
    }

    public function scopeTotalRating($query)
    {
    	return $query->where('user_id', Auth::user()->id);
    }

    public function scopeTotalRatingDynamic($query, $user)
    {
    	return $query->where('user_id', $user);
    }
}

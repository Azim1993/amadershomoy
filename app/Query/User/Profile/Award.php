<?php

namespace App\Query\User\Profile;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Award extends Model
{
    protected $fillable = ['title', 'award_date', 'link', 'note'];
    
    protected $dates = ['award_date'];

    public function setAwardDateAttribute( $value ) {
        $this->attributes['award_date'] = Carbon::parse($value)->format('d/m/y');
    }

	public function getAwardDateAttribute( $value ) {
  		return $this->attributes['award_date'] = Carbon::parse($value)->toFormattedDateString();
	}
    public function user()
    {
    	return $this->belongsTo(\App\Auth\User::class);
    }
}

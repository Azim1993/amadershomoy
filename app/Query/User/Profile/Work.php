<?php

namespace App\Query\User\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Work extends Model
{
	protected $fillable = ['org_name', 'position', 'from', 'to', 'current'];
    
    protected $dates = ['from', 'to'];
    //"2018-01-20T03:46:58.000Z"

    public function setFromAttribute($date) {
        $this->attributes['from'] = $this->dateFormat($date);
    }

    // $this->attributes['from'] = Carbon::parse($date)->format('d/m/y');
    // $this->attributes['from'] = Carbon::createFromFormat('d F,Y', $date);
    public function setToAttribute($date) {
        $this->attributes['to'] = $this->dateFormat($date);
    }

    private function dateFormat($data)
    {
        $value =  explode('-',$data);
        $edate = explode('T', $value[2]);
        $value[2] = $edate[0];
        return implode('-', $value);
    }
    
	public function getFromAttribute( $value ) {
  		return $this->attributes['from'] = Carbon::parse($value)->toFormattedDateString();
	}

	public function getToAttribute( $value ) {
  		return $this->attributes['to'] = Carbon::parse($value)->toFormattedDateString();
	}

	public function workCreate($data)
	{
    	return self::create($data);
	}

    public function user()
    {
    	return $this->belongsTo(\App\Auth\User::class);
    }

}

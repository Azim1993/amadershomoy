<?php

namespace App\Query\User\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ProfileImage extends Model
{
    protected $fillable = ['user_id', 'type', 'image', 'status'];

    /**
     *  array [
     *      cover => 1,
     *      avater => 2
     *  ];
     */

    public function user()
    {
        return $this->belongsTo(\App\Auth\User::class);
    }

    public function avatarStore($fileName)
    {
        return static::create([
            'user_id' => Auth::user()->id,
            'type' => 2,
            'image' => $fileName,
            'status' => 1
        ]);
    }

    public function coverStore($fileName)
    {
        return static::create([
            'user_id' => Auth::user()->id,
            'type' => 1,
            'image' => $fileName,
            'status' => 1
        ]);
    }

    public static function scopeCovers($query)
    {
        return $query->where(['type' => 1]);
    }

    public static function scopeAvatars($query)
    {
    	return $query->where(['type' => 2]);
    }
    /**
     * Curretn Cover query
     * @param  object $query
     * @return array
     */
    public static function scopeCover($query)
    {
        return $query->where([
                'type' => 1,
                'status' => 1
                ]);
    }
    /**
     * Current Avater query
     * @param  object $query 
     * @return array        
     */
    public static function scopeAvatar($query)
    {
    	return $query->where([
    			'type' => 2,
    			'status' => 1
    			]);
    }

    /**
     *  db trigger for cover create
     */
    public function scopeCoverTrigger($query)
    {   
        return $query->where([
                'user_id' => Auth::user()->id,
                'type' => 1,
                'status' => 1
                ]);
    }

    /**
     *  db trigger for Avater create
     */
    public function scopeAvaterTrigger($query)
    {   
        return $query->where([
                'user_id' => Auth::user()->id,
                'type' => 2,
                'status' => 1
                ]);
    }
}

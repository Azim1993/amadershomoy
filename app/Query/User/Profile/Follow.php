<?php

namespace App\Query\User\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Follow extends Model
{
	protected $fillable = ['user_id', 'follows_id'];
	
	public function followStore($user)
	{
		return static::create([
			'user_id' => Auth::user()->id,
			'follows_id' => $user->id
		]);
	}

	public function user()
    {
        return $this->belongsTo(\App\Query\Auth\User::class);
    }

	public function follower()
    {
        return $this->belongsTo(\App\Query\Auth\User::class, 'follows_id');
    }

    public function scopeAllFollower($query)
    {
    	return $query->where('user_id', Auth::user()->id);
    }

    public function scopeAllFollowing($query)
    {
    	return $query->where('follows_id', Auth::user()->id);
    }
}

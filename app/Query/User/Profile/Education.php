<?php

namespace App\Query\User\Profile;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $fillable = [
    	'institude','certificate_name', 'major', 'from', 'to', 'current_stady'
    ];
    protected $dates = ['from', 'to'];

    public function user()
    {
    	return $this->belongsTo(\App\Auth\User::class);
    }
    
    public function setFromAttribute($date) {
        $this->attributes['from'] = $this->dateFormat($date);
    }

    public function setToAttribute($date) {
        $this->attributes['to'] = $this->dateFormat($date);
    }

    private function dateFormat($data)
    {
        $value =  explode('-',$data);
        $edate = explode('T', $value[2]);
        $value[2] = $edate[0];
        return implode('-', $value);
    }
    
	public function getFromAttribute( $value ) {
  		return $this->attributes['from'] = Carbon::parse($value)->toFormattedDateString();
	}

	public function getToAttribute( $value ) {
  		return $this->attributes['to'] = Carbon::parse($value)->toFormattedDateString();
	}

	public function educationCreate($data)
	{
    	return self::create($data);
	}

}

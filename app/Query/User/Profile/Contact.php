<?php

namespace App\Query\User\Profile;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	protected $fillable = ['type', 'link'];
    
    public function store($data)
    {
    	return self::create($data);
    }

    public function getTypeAttribute($value)
    {
    	return $this->attributes['type'] = config('socialLinks.links.'.$value);
    }

    public function user()
    {
    	return $this->belongsTo(\App\Auth\User::class);
    }
}

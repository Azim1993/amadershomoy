<?php

namespace App\Query\User\Profile;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class BasicInfo extends Model
{
    protected $fillable = [
    	'user_id', 'first_name', 'last_name', 'gender', 'blood', 'birth_date', 'study', 'profession', 'district', 'address'
    ];

    protected $dates = ['birth_date'];

    public function setBirthDateAttribute( $value ) {
        $this->attributes['birth_date'] = Carbon::parse($value);
    }
    
	// public function getBirthDateAttribute( $value ) {
 //  		return $this->attributes['birth_date'] = Carbon::parse($value)->toFormattedDateString();
	// }

    public function user()
    {
    	return $this->belongsTo(\App\Auth\User::class);
    }

    public function store($user, $data)
    {
    	return self::create($data + ['user_id' => $user]);
    }
}

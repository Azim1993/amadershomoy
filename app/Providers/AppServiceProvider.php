<?php

namespace App\Providers;

use App\Repository\Admin\AdminGroupEloq;
use App\Repository\Admin\AdminGroupRepo;
use App\Repository\Admin\Emergency\EmergencyContactEloq;
use App\Repository\Admin\Emergency\EmergencyContactRepo;
use App\Repository\Admin\User\UserControllEloq;
use App\Repository\Admin\User\UserControllRepo;
use App\Repository\Auth\UserRegisterEloq;
use App\Repository\Auth\UserRegisterRepo;
use App\Repository\EloquentUser;
use App\Repository\Post\UserPostEloq;
use App\Repository\Post\UserPostRepo;
use App\Repository\UserRepository;
use App\Repository\User\Anononus\AnononusUserEloq;
use App\Repository\User\Anononus\AnononusUserRepo;
use App\Repository\User\AuthorizeUserEloq;
use App\Repository\User\AuthorizeUserRepo;
use App\Repository\User\Group\UserGroupEloq;
use App\Repository\User\Group\UserGroupRepo;
use App\Repository\User\Network\UserNetworkEloq;
use App\Repository\User\Network\UserNetworkRepo;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserRegisterRepo::class, UserRegisterEloq::class);
        $this->app->singleton(AuthorizeUserRepo::class, AuthorizeUserEloq::class);
        $this->app->singleton(AnononusUserRepo::class, AnononusUserEloq::class);
        $this->app->singleton(UserPostRepo::class, UserPostEloq::class);
        $this->app->singleton(UserRepository::class, EloquentUser::class);
        $this->app->singleton(UserGroupRepo::class, UserGroupEloq::class);
        $this->app->singleton(UserNetworkRepo::class, UserNetworkEloq::class);

        /**
         * Admin repository service provider
         */
        $this->app->singleton(AdminGroupRepo::class, AdminGroupEloq::class);
        $this->app->singleton(EmergencyContactRepo::class, EmergencyContactEloq::class);
        $this->app->singleton(UserControllRepo::class, UserControllEloq::class);
    }
}

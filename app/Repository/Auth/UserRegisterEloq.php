<?php 

namespace App\Repository\Auth;

use App\Query\Auth\User;
use App\Query\User\Profile\BasicInfo;
use App\Repository\Auth\UserRegisterRepo;

class UserRegisterEloq implements UserRegisterRepo {

	private $user;
	private $basicInfo;
	/**
	 * user data model 
	 * @param User      $user      [description]
	 * @param BasicInfo $basicInfo [description]
	 */
	function __construct(User $user, BasicInfo $basicInfo)
	{
		$this->user 	 = $user;
		$this->basicInfo = $basicInfo;
	}

	public function storeUserAndBasic(array $request)
	{
		\DB::transaction(function () use ($request) {
			$this->user = $this->user->register($request);
			$this->basicInfo->store($this->user->id, $request);
		}, 2);
		return $this->user;
	}

	public function sendActivationLink()
	{
		return true;
	}

}
<?php 
namespace App\Repository\Auth;

interface UserRegisterRepo {
	/**
	 * user singup form request
	 * @param  array  $request [description]
	 * @return [type]          
	 */
	public function storeUserAndBasic(array $request);
	/**
	 * set activation link to user
	 * @return boolean 
	 */
	public function sendActivationLink();

}
<?php 

namespace App\Repository\Admin;

interface AdminGroupRepo {
	/**
	 * [getAllGroup description]
	 * @return array 
	 */
	public function getAllGroup();

	public function jsonGroup();
	
	public function getChildGroups();
	
	public function getAllGroupWithChild();

	public function storeGroup($data);
}
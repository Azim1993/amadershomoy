<?php 

namespace App\Repository\Admin;

use App\Query\Admin\Post\Group\Group;
use App\Repository\Admin\AdminGroupRepo;
use Illuminate\Support\Facades\Storage;

class AdminGroupEloq implements AdminGroupRepo {
	private $group;

	public function __construct(Group $group)
	{
		$this->group = $group;
	}

	/**
	 * [getAllGroup description]
	 * @return array 
	 */
	public function getAllGroup()
	{
		return $this->group
		            ->parent()
                    ->orderBy('created_at','asc')
                    ->paginate(20);
	}

	public function jsonGroup()
	{
		return $this->group
		            ->parent()
                    ->orderBy('created_at','asc')
                    ->get();
	}

	public function getChildGroups()
	{
		return $this->group
		            ->childGroup()
		            ->with('parentTo')
                    ->orderBy('created_at','asc')
                    ->paginate(30);
	}

	public function getAllGroupWithChild()
	{
		return $this->group
		            ->parent()
		            ->with('child','child.parentTo')
                    ->orderBy('created_at','asc')
                    ->paginate(30);
	}


	public function storeGroup($data)
	{
		if($data->has('logoFile')) {
			$logo = Storage::disk('public')
			    ->put('admin/group/logo', $data->file('logoFile'));
			$data['logo'] = $logo;
		}
		if($data->has('coverFile')) {
			$cover = Storage::disk('public')
			    ->put('admin/group/cover', $data->file('coverFile'));
			$data['cover'] = $cover;
		}
		if($this->group->store($data->all()))
			return true;
		return false;
	}

}
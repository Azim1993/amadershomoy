<?php 

namespace App\Repository\Admin\User;

use App\Query\Auth\User;
use App\Repository\Admin\User\UserControllRepo;

class UserControllEloq implements UserControllRepo {
	private $user;

	public function __construct(User $user)
	{
		$this->user = $user;
	}



	public function users()
	{
		return $this->user->with('basicInfo')->get();
	}
}
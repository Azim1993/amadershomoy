<?php 

namespace App\Repository\Admin\Emergency;

interface EmergencyContactRepo {

	public function services();
	public function serviceStore($requestData);
	public function serviceUpdate($service, $name);
	public function serviceDelete($service);

	public function contactByService($service);
	public function contactStore($data);
	public function contactUpdate($contact, $data);
	public function contactDelete($contact);
}
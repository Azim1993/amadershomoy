<?php 

namespace App\Repository\Admin\Emergency;

use App\Query\Admin\Emergency\EmergencyContact;
use App\Query\Admin\Emergency\Service;
use App\Repository\Admin\Emergency\EmergencyContactRepo;

class EmergencyContactEloq implements EmergencyContactRepo {
	private $service;
	private $contact;

	public function __construct(Service $service,EmergencyContact $contact)
	{
		$this->service = $service;
		$this->contact = $contact;
	}




	public function services()
	{
		return $this->service->with('contacts')->orderBy('created_at','desc')->get();
	}

	public function serviceStore($requestData)
	{
		return $this->service->create($requestData);
	}

	public function serviceUpdate($service, $name)
	{
		throw new \Exception('Method serviceUpdate() is not implemented.');
	}

	public function serviceDelete($service)
	{
		throw new \Exception('Method serviceDelete() is not implemented.');
	}

	public function contactByService($service)
	{
		return $this->service->with(['contacts' => function($q){
			return $q->with('districtName','upazilaName')->orderBy('district','asc');
		}])->find($service);
	}

	public function contactStore($data)
	{
		return $this->contact->create([
            'service_id' => $data['service_id'],
            'district' => $data['district'],
            'upazila' => $data['upazila'],
            'location' => $data['location'],
            'contact_number' => $data['contact_number']
		]);
	}

	public function contactUpdate($contact, $data)
	{
		throw new \Exception('Method contactUpdate() is not implemented.');
	}

	public function contactDelete($contact)
	{
		throw new \Exception('Method contactDelete() is not implemented.');
	}
}
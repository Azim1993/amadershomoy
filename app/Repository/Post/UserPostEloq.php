<?php
namespace App\Repository\Post;

use App\Query\Post\Asset;
use App\Query\Post\Blog;
use App\Repository\Post\PostFileUpload;
use App\Repository\Post\UserPostRepo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserPostEloq implements UserPostRepo{

	private $post;
	private $postAsset;

	function __construct(Blog $post, Asset $postAsset)
	{
		$this->post = $post;
		$this->postAsset = $postAsset;
	}

	public function getUserPosts()
	{
		return $this->post->where('user_id', Auth::user()->id)
					->with('user.postUser','likes','shares','comments','assets')
					->orderBy('created_at','desc')->get();
	}
	/**
	 * store user post message and files
	 * @param  object  $postData 
	 * @return object
	 */
	public function store($postData)
	{
		if($message = $postData['message']) {
			$this->post = $this->post->store($message);
		}
		return $this->post;
	}

	/**
	 * store post files
	 * @param  array $file 
	 * @return [type]       [description]
	 */
	public function fileStore(array $files)
	{
		$this->fileMove($this->post, $files);
    	return true;
	}

	private function fileMove($post, $files)
	{
    	$path = public_path('/images/post/');
    	foreach ($files as $key => $file) {
	    	$ext = $file->getClientOriginalExtension();
	    	$fileName = Storage::disk('public')->put('post/'.$post->id, $file);
	    	$this->fileDataStore($fileName, $ext);
    	}
    	return true;
	}

	private function fileDataStore($fileName,$ext)
	{
		$this->postAsset->create([
			'user_id' => Auth::user()->id,
			'blog_id' => $this->post->id,
			'type' => $this->fileType($ext),
			'fileName' => $fileName
		]);
		return true;
	}

	private function fileType($ext)
	{
		return in_array($ext, ['png','jpg','jpeg','svg','bmp','gif'])?1:2;
	}
}
<?php 

namespace App\Repository\Post;

interface UserPostRepo {
	/**
	 * get user post 
	 * @return [array]
	 */
	public function getUserPosts();
	/**
	 * store user post message and files
	 * @param  object  $postData 
	 * @return object
	 */	
	public function store($postData);

	/**
	 * store post files
	 * @param  array $file 
	 * @return [type]       [description]
	 */
	public function fileStore(array $file);
}
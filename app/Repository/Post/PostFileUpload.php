<?php 

namespace App\Repository\Post;

use App\Repository\Utility\FileUploadTrait;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class PostFileUpload
{
	use FileUploadTrait;
	public function imageUpload($post, $images, $storePath)
	{
		if($images == null)
			return false;

		if(!is_array($images)) {
			$images[0] = $images;
		}
		foreach($images as $image) {
			if($movedFile = $this->fileMoved($image, $storePath)) {
				$this->store($post, $movedFile);
			}
		}
		return true;
	}

	private function store($post,array $movedFile)
	{
		return $post->assets()
			->create($movedFile + ['user_id' => Auth::user()->id]);
	}

	private function fileMoved($file, $path)
	{
		$type = $this->base64Type($file);
		$name = Carbon::now().'.'.$type;
		$movedFile = $this->base64_to_jpeg($file, public_path($path.$name));

		return [
			'type' => $type=='png'?1:2,
			'fileName' => $name
		];
	}
}
<?php 

namespace App\Repository;

use App\Query\Auth\User;

class EloquentUser implements UserRepository
{
	private $user;

	function __construct(User $user)
	{
		$this->user = $user;
	}

	public function allUser()
	{
		return $this->user->all();
	}
}
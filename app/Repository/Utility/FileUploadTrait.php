<?php 

namespace App\Repository\Utility;

trait FileUploadTrait {

	/**
	 * file type from base64 file
	 * @param  [type] $value from base 64 file
	 * @return string
	 */	
    private function base64Type($value)
    {
		$parse  = strpos($value, ';');
		$mineAndType = explode(':', substr($value, 0, $parse))[1];
		$mineAndTypeParse = explode('/',$mineAndType);
		$type = $mineAndTypeParse[1];
		return $type;
    }

    public function base64_to_jpeg($base64_string, $output_file) {
	    // open the output file for writing
	    $ifp = fopen( $output_file, 'wb' ); 

	    // split the string on commas
	    // $data[ 0 ] == "data:image/png;base64"
	    // $data[ 1 ] == <actual base64 string>
	    $data = explode( ',', $base64_string );

	    // we could add validation here with ensuring count( $data ) > 1
	    fwrite( $ifp, base64_decode( $data[ 1 ] ) );

	    // clean up the file resource
	    fclose( $ifp ); 

	    return $output_file; 
	}
}
<?php 

namespace App\Repository\User\Group;

use App\Query\Admin\Post\Group\Group;
use App\Query\Admin\Post\Group\GroupPost;
use App\Query\Admin\Post\Group\GroupUser;
use App\Query\Admin\Post\Group\Utility\GroupPostAsset;
use App\Query\Poll\Poll;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserGroupEloq implements UserGroupRepo {
    private $userGroup;
    private $groupPost;
    private $group;
    private $asset;
    private $poll;

    public function __construct(
        GroupUser $userGroup,
        GroupPost $groupPost,
        GroupPostAsset $asset,
        Group $group,
        Poll $poll
    )
    {
        $this->userGroup = $userGroup;
        $this->groupPost = $groupPost;
        $this->group = $group;
        $this->asset = $asset;
        $this->poll = $poll;
    }

    public function getGroupPost($group)
    {
        return $this->groupPost->where('group_id', $group)
            ->with('polls','assets')
            ->with(['user' => function($query) {
                return $query->with('postUser','currentProfile');
            }])
            ->orderBy('created_at','desc')
            ->paginate(30);
    }

    public function userJoinGroup($data)
    {
        if($this->group->find($data['group_id']))
            return $this->userGroup
                    ->create($data + ['user_id' => Auth::user()->id]);
        return false;
    }

    public function getUserGroups()
    {
        return Auth::user()
            ->groups->load(['group' => function($q) {
            return $q->with('parentTo','posts');
        }]);
    }

    public function getParentGroups()
    {
        return $this->group
                    ->parent()
                    ->orderBy('created_at','asc')
                    ->get();
    }

    public function groupInfoJson($group)
    {
        return $this->group->findOrFail($group);
    }

    public function groupStore($groupId, $message)
    {
        if($this->group = $this->groupInfoJson($groupId))
            return $this->groupPost = $this->groupPost
                ->storePost($groupId, $message);
        return false;
    }

    public function pollStore(array $polls)
    {
        foreach ($polls as $poll) {
            if($poll == null)
                return false;
            $this->poll->store($this->groupPost->id, $poll);
        }
        return true;
    }

    public function fileStore(array $files)
    {
        $this->fileMove($this->groupPost, $files);
        return true;
    }

    private function fileMove($post, $files)
    {
        $path = public_path('/images/group/');
        foreach ($files as $key => $file) {
            $ext = $file->getClientOriginalExtension();
            $fileName = Storage::disk('public')->put('group/'.$this->group->id.'/post/'.$post->id, $file);
            $this->asset->store($post->id, $fileName, $ext);
        }
        return true;
    }
}
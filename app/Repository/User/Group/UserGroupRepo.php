<?php 

namespace App\Repository\User\Group;

interface UserGroupRepo {
    public function getGroupPost($group);

    public function userJoinGroup($request);
    
    public function getUserGroups();

    public function getParentGroups();

    public function groupInfoJson($group);

    public function groupStore($groupId, $requestData);

    public function fileStore(array $postFiles);

    public function pollStore(array $polls);
}
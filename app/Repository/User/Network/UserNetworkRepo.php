<?php 

namespace App\Repository\User\Network;

interface UserNetworkRepo {

	public function authorizeUserFollowing();

	public function followCreate($user);

}
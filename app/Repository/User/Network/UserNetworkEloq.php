<?php 
namespace App\Repository\User\Network;

use App\Query\Auth\User;
use App\Query\User\Profile\Follow;

class UserNetworkEloq implements UserNetworkRepo {

	private $network;
	private $user;

	public function __construct(Follow $network,User $user)
	{
		$this->network = $network;
		$this->user = $user;
	}

	public function followCreate($user)
	{
		if($checkUser = $this->user->find($user))
			return $this->network->followStore($checkUser);
		return response()->json('We do not find any user for you');
	}

	public function authorizeUserFollowing()
	{
		return $this->network->allFollower()
				->with(['follower' => function($user) {
					return $user->with('postUser','currentProfile');
				}])
				->get();
	}
	
	// ->with(['user' => function($user) {
	// 	return $user->with('postUser','currentProfile');
	// }])
}
<?php 

namespace App\Repository\User;

use App\Query\User\Profile\ProfileImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AuthorizeUserEloq implements AuthorizeUserRepo {
	private $profileImages;

	function __construct(ProfileImage $images)
	{
		$this->profileImages = $images;
	}
	/**
	 * Get all information of a login user
	 * @return array 
	 */
	public function info()
	{
		return Auth::user()
				->load('basicInfo','awards','contact','currentProfile','currentCover');
	}

	public function storeAvatar($file)
	{
		$images = $this->profileImages->avaterTrigger();
		if($images->count() > 0) {
			$images->update(['status' => 0]);
		}
    	$fileName = Storage::disk('public')->put('avater/'.Auth::user()->id, $file);
		return $this->storeAvatarDB($fileName);
	}

	public function storeCover($file)
	{
		$images = $this->profileImages->coverTrigger();
		if($images->count() > 0) {
			$images->update(['status' => 0]);
		}
    	$fileName = Storage::disk('public')->put('cover/'.Auth::user()->id, $file);
		return $this->storeCoverDB($fileName);
	}

	private function storeAvatarDB($fileName)
	{
		return $this->profileImages->avatarStore($fileName);
	}

	private function storeCoverDB($fileName)
	{
		return $this->profileImages->coverStore($fileName);
	}
}
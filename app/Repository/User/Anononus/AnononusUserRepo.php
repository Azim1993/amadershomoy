<?php 
namespace App\Repository\User\Anononus;

interface AnononusUserRepo {
	
	public function userInfo(int $user);

	public function rateStore(int $rateTo, $data);
}
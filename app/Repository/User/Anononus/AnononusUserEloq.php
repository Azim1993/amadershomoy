<?php 

namespace App\Repository\User\Anononus;

use App\Query\Auth\User;
use App\Query\User\Rating;
use App\Repository\User\Anononus\AnononusUserRepo;

class AnononusUserEloq implements AnononusUserRepo {

	private $user;
	private $rate;

	public function __construct(User $user,Rating $rate)
	{
		$this->user = $user;
		$this->rate = $rate;
	}



	public function userInfo(int $user)
	{
		return $this->user->find($user)->load('basicInfo','awards','contact','currentProfile','currentCover');
	}

	public function rateStore(int $rateTo, $data)
	{
		if($this->rate->checkRate($rateTo)->count() === 0)
			return $this->rate->store($rateTo, $data);
		return 'You already rate this person';
	}
}
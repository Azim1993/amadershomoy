<?php 

namespace App\Repository\User;

interface AuthorizeUserRepo {
	public function info();

	public function storeAvatar($file);
	public function storeCover($file);
}
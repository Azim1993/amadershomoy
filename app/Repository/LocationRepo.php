<?php 

namespace App\Repository;

use App\Query\Location\District;
use App\Query\Location\Upazila;

class LocationRepo {
	
	public function getDistrict()
	{
		return District::with('upazilas')->get();
	}

	public function getUpazila()
	{
		return Upazila::all();
	}
}
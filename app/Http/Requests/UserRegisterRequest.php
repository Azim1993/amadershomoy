<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user() ? false : true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'     => 'required|string|email|max:100|unique:users',
            'password'  => 'required|string|min:6|confirmed',
            'first_name'=> 'required|string|max:50',
            'last_name' => 'required|string|max:50',
            'gender'    => 'required|string|max:20',
            'blood'     => 'required|string|max:20',
            'phone'     => 'required|string|max:100|unique:users',
            'user_name' => 'string|nullable|unique:users',
            'birth_date'=> 'required|date|max:50',
            'study'     => 'required|string|max:100',
            'profession'=> 'required|integer|max:100',
            'district'  => 'required|integer',
            'address'   => 'required|string',
        ];
    }
}

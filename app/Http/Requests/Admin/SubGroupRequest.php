<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SubGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
     public function authorize()
    {
        return $this->user()?true:false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:groups|string|max:100',
            'group_parent_id' => 'required|integer',
            'detail' => 'nullable',
            'logoFile' => 'nullable|image|max:10240',
            'coverFile' => 'nullable|image|max:10240',
        ];
    }
    public function messages()
    {
    return [
        'name.required' => 'A Sub-Group name is required',
        'group_parent_id.required'  => 'A parent group is required',
    ];
}
}

<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class WorkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()?true:false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'org_name' => 'required|string|max:150',
            'position' => 'required|string|max:100',
            'from'     => 'required|date',
        ];

        if($this->current == null) {
            $rules['to'] = 'required|date';
        } else {
            $rules['current']  = 'required|boolean';
        }

        return $rules;
    }
}

<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class EducationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()?true:false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        $rules = [
            'institude' => 'required|string|max:150',
            'certificate_name' => 'required|string|max:100',
            'major'     => 'required|string',
            'from'     => 'required|date',
        ];

        if($this->current_stady == null) {
            $rules['to'] = 'required|date';
        } else {
            $rules['current_stady']  = 'required|boolean';
        }

        return $rules;
    }
}

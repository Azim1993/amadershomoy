<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\Admin\User\UserControllRepo;
use Illuminate\Http\Request;

class UserController extends Controller
{
	private $user;

	public function __construct(UserControllRepo $user)
	{
		$this->user = $user;
	}

	public function index()
	{
		$users = $this->user->users();
		// return $users;
		return view('admin.user.user', compact('users'));
	}
    
}

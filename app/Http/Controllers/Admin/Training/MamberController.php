<?php

namespace App\Http\Controllers\Admin\Training;

use App\Http\Controllers\Controller;
use App\Query\Admin\Training\Training;
use App\Query\Admin\Training\TrainingUser;
use App\Query\Auth\User;
use Illuminate\Http\Request;

class MamberController extends Controller
{
    public function index(Training $training)
    {
    	$training = $training->load('users.basicInfo');
    	return view('admin.training.members', compact('training'));
    }

    public function allUserSearch(Request $request)
    {
		// if($request->ajax()){
	    	$users = User::where('email','LIKE','%'.$request->search."%")
	    	    ->with('fullName')
	    	    ->get();
	    	// $users['name'] = $users->basicInfo->first_name. ' ' . $users->basicInfo->last_name;
	    	return $users;
		// }
    }
}

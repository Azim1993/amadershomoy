<?php

namespace App\Http\Controllers\Admin\Training;

use App\Http\Controllers\Controller;
use App\Query\Admin\Training\Training;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TrainingController extends Controller
{
	private $training;

	public function __construct(Training $training)
	{
		$this->training = $training;
	}


    public function index()
    {
    	$trainings = $this->training->with('users')->orderBy('created_at','desc')->paginate(30);
    	// return $trainings;
    	return view('admin.training.training', compact('trainings'));
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required|max:200',
    		'logo' => 'nullable|max:2000|image'
    	]);
    	$image = null;
    	if($request->file('logo')) {
    		$image = Storage::disk('public')->put('admin/training', $request->file('logo'));
    	}
    	$this->training->create([
    		'name' => $request->input('name'),
    		'logo' => $image,
    	]);
    	return back()->with('success','training store successfully');
    }
}

<?php

namespace App\Http\Controllers\Admin\Emergency;

use App\Http\Controllers\Controller;
use App\Repository\Admin\Emergency\EmergencyContactRepo;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $service;

    public function __construct(EmergencyContactRepo $service)
    {
        $this->service = $service;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = $this->service->services();
        return view('admin.emergency.service', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|max:200']);

        $this->service->serviceStore($request->all());

        return back()->with('success','Service store successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function allContact($service)
    {
         $contact = $this->service->contactByService($service);
         $services = $this->service->services();
        return view('admin.emergency.contact', compact('contact','services'));
    }

    public function contactStore(Request $request)
    {
        $this->validate($request,[
            'service_id' => 'required|integer',
            'district' => 'required|integer',
            'upazila' => 'required|integer',
            'location' => 'required',
            'contact_number' => 'required'
        ]);

        $this->service->contactStore($request->all());

        return back()->with('success','contact store successfully');
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRegisterRequest;
use App\Repository\Auth\UserRegisterRepo;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    protected $user;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRegisterRepo $user)
    {
        $this->middleware('guest');
        $this->user = $user;
    }
    
    public function register(UserRegisterRequest $request)
    {
        if($user = $this->user->storeUserAndBasic($request->all()))
        {
            $this->user->sendActivationLink();

            return response()->json([
                'user' => $user,
                'redirect' => route('login')
            ]);
        }
        return false;
    }
}

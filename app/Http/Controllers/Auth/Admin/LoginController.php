<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin');
    }
    public function show()
    {
    	return view('auth.adminLogin');
    }

    public function loginStore(Request $request)
    {
    	$this->loginValidation($request);

        if(Auth::guard('admin')->attempt(['email'=>$request->email, 'password' => $request->password], $request->remember))
        {
            return  redirect()->intended(route('admin.dashboard'));
        }
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

    public function loginValidation($data)
    {
    	return $this->validate($data,[
    		'email' => 'required',
    		'password'	=> 'required|min:6'
    		]);
    }
}

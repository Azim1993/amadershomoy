<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Query\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    public function home()
    {
        if(Auth::check())
            return view('user.dashboard');
        return view('welcome');
    }
    public function profile()
    {
    	return view('user.home');
    }
    public function updateProfile()
    {
    	return view('user.profile.profile');
    }
    public function anonymous($user)
    {
        if($user = User::find($user))
    	   return view('user.anonymous', compact('user'));
        return redirect('/')->with('warning','Do not find any user');
    }
    public function network()
    {
    	return view('user.network');
    }
}

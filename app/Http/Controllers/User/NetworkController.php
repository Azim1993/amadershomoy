<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Repository\User\Network\UserNetworkRepo;
use Illuminate\Http\Request;

class NetworkController extends Controller
{
	private $userNetwork;

	public function __construct(UserNetworkRepo $userNetwork)
	{
		$this->userNetwork = $userNetwork;
	}


    public function index()
    {
    	return response()->json($this->userNetwork->authorizeUserFollowing());
    }

    public function store($user, Request $request)
    {
    	return response()->json($this->userNetwork->followCreate($user));
    }
}

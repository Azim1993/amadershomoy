<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Repository\User\Group\UserGroupRepo;
use Illuminate\Http\Request;

class GroupController extends Controller
{
      private $userGroup;

    public function __construct(UserGroupRepo $userGroup)
    {
        $this->userGroup = $userGroup;
    }

    public function index() 
    {
        return view('user.group.index');
    }

    public function groupPost($group)
    {
        return response()->json($this->userGroup->getGroupPost($group));
    }
    public function show($group)
    {
        return view('user.group.show',compact('group'));
    }

    public function store($groupId, Request $request)
    {
        $this->userGroup->groupStore($groupId, $request->message);
        $this->userGroup->fileStore($request->file);
        $this->userGroup->pollStore($request->poll);
        return response()->json($this->userGroup);
    }

    public function groupInfo(Request $request)
    {
        return $this->userGroup->groupInfoJson($request->group_id);
    }

    public function allGroup()
    {
        return $this->userGroup->getParentGroups();
    }

    public function userAllGroup()
    {
        return $this->userGroup->getUserGroups();
    }

    public function join(Request $request)
    {
        $this->validate($request,['group_id' => 'required|integer']);
        
        return $this->userGroup->userJoinGroup($request->all());
    }
}

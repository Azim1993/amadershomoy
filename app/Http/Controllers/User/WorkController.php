<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\WorkRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WorkController extends Controller
{

    public function index()
    {
    	return response()->json($this->user()->works()->get());
    }
    
    public function store(WorkRequest $request)
    {
    	return response()->json(
    		$this->user()->works()->create($request->all())
    	);
    }
    private function user()
    {
        return Auth::user();
    }
}

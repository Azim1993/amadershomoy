<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\PostStoreRequest;
use App\Repository\Post\UserPostRepo;
use Illuminate\Http\Request;

class PostController extends Controller
{
	private $postRepo;

	function __construct(UserPostRepo $postRepo)
	{
		$this->postRepo = $postRepo;
	}

    public function index()
    {
        return $this->postRepo->getUserPosts();
    }

    public function store(PostStoreRequest $request)
    {
    	$this->postRepo->store($request->all());
    	$this->postRepo->fileStore($request->file('file'));
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Repository\User\AuthorizeUserRepo;
use Illuminate\Http\Request;

class AuthorizeUserController extends Controller
{
    public $user;

    function __construct(AuthorizeUserRepo $user)
    {
    	$this->user = $user;
    }

    public function userInfo()
    {
    	// return view('home', ['info' => $this->user->info()]);
    	return response()->json($this->user->info());
    }

    public function avatar(Request $request)
    {
    	if($request->has('avatar'))
    		return $this->user->storeAvatar($request->file('avatar'));
    	return 'no Profile Image found';
    }

    public function cover(Request $request)
    {
    	if($request->has('cover'))
    		return $this->user->storeCover($request->file('cover'));
    	return 'no Cover Image found';
    }
}

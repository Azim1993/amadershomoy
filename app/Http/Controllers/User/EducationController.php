<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\EducationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EducationController extends Controller
{

    public function index()
    {
    	return response()->json($this->user()->educations()->get());
    }
    
    public function store(EducationRequest $request)
    {
    	return response()->json(
    		$this->user()->educations()->create($request->all())
    	);
    }

    private function user()
    {
    	return Auth::user();
    }
}

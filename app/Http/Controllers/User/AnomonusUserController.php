<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Repository\User\Anononus\AnononusUserRepo;
use Illuminate\Http\Request;

class AnomonusUserController extends Controller
{
	public $user;

    public function __construct(AnononusUserRepo $user)
    {
    	$this->user = $user;
    }


    public function show($user)
    {
    	return response()->json($this->user->userInfo($user));
    }

    public function rate($user, Request $request)
    {
    	$this->validate($request, ['rate' => 'required|integer']);
    	return response()->json(
    		$this->user->rateStore($user, $request->rate)
    	);
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\AwardRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AwardController extends Controller
{

    public function index()
    {
    	return response()->json($this->user()->awards()->get());
    }
    
    public function store(AwardRequest $request)
    {
    	return response()->json(
    		$this->user()->awards()->create($request->all())
    	);
    }
    private function user()
    {
        return Auth::user();
    }
}

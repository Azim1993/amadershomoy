<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SocialController extends Controller
{
	public function index()
    {
    	return response()->json($this->user()->contact()->get());
    }
    
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'type' => 'required|integer',
    		'link' => 'required|string|max: 255',
    	]);

    	return response()->json(
    		$this->user()->contact()->create($request->all())
    	);
    }

    public function available()
    {
    	return response()->json(config('socialLinks.links'));
    }

    private function user()
    {
        return Auth::user();
    }
}

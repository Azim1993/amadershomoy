<?php

namespace App\Http\Controllers;

use App\Repository\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
	private $user;

	public function __construct(UserRepository $user)
	{
		$this->user = $user;
	}

    public function all()
    {
    	return $this->user->allUser();
    }

    public function authorizeUserInfo()
    {
    	return response()->json(
    		$this->user->authorizeInfo()
    	);
    }
}

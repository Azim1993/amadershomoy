<?php
Route::get('/', 'User\PageController@home')->name('welcome');
Auth::routes();
Route::namespace('User')->middleware(['auth'])->group(function() {
	Route::get('/profile', 'PageController@profile')->name('user.profile');
	Route::get('/profile/update', 'PageController@updateProfile')->name('user.profile.update');
	Route::post('/avatar/update', 'AuthorizeUserController@avatar')->name('user.avatar.store');
	Route::post('/cover/update', 'AuthorizeUserController@cover')->name('user.cover.store');

	Route::get('/profile/{anonymous}', 'PageController@anonymous')->name('user.profile.anonymous');
	Route::get('/user/{user}/profile', 'AnomonusUserController@show')->name('user.anonymous.json');
	Route::post('/user/{user}/rate', 'AnomonusUserController@rate')->name('user.rate.store');

	Route::get('/authorize_info', 'AuthorizeUserController@userInfo')->name('user.auth.info');
	Route::get('/posts', 'PostController@index')->name('post.index');
	Route::post('/post/store', 'PostController@store')->name('post.store');
	Route::get('/basicInfo', 'ProfileController@show');
	Route::resource('/works', 'WorkController');
	Route::resource('/educations', 'EducationController');
	Route::resource('/socials', 'SocialController');
	Route::resource('/awards', 'AwardController');
	Route::get('/available_links', 'SocialController@available')->name('social.set.links');

	Route::get('/network', 'PageController@network')->name('user.profile.network');
	Route::post('/network/{user}/store', 'NetworkController@store')->name('user.network.store');
	Route::get('/authorize_user_network_json', 'NetworkController@index');

	Route::get('/groups', 'GroupController@index')->name('user.groups');
	Route::post('/groups/join', 'GroupController@join')->name('user.group.join');
	Route::get('/group/{group}', 'GroupController@show')->name('user.groups.show');
	Route::get('/groups_json', 'GroupController@allGroup')->name('groups.json');
	Route::get('/group_json', 'GroupController@groupInfo')->name('group.info.json');
	Route::get('/group/{group}/post_json', 'GroupController@groupPost')->name('user.groups.posts');
	Route::get('/user_groups_json', 'GroupController@userAllGroup')->name('user.groups.json');
	Route::Post('/group/{group}/post_store', 'GroupController@store')->name('group.post.store');

});
/*|=====| User guest routes |==============*/

/*|=====| Admin guest routes |==============*/
Route::namespace('Auth')->group(function() {
	Route::get('/admin/login', 'Admin\LoginController@show')->name('admin.login.show');
	Route::post('/admin/login', 'Admin\LoginController@loginStore')->name('admin.login.store');
});
Route::namespace('Admin')->middleware(['auth:admin'])->prefix('admin')->group(function() {
	Route::get('/dashboard','PageController@dashboard')->name('admin.dashboard');
	Route::get('/users', 'UserController@index')->name('admin.user.all');
	Route::get('/blocked_users', 'UserController@blokedUser')->name('admin.user.blocked');
	Route::get('/user/{user}/block', 'UserController@index')->name('admin.user.store.block');
	Route::post('/user/{user}/waring', 'UserController@blockStore')->name('admin.user.warning.store');
	Route::post('/user/{user}/block', 'UserController@blockStore')->name('admin.user.block.store');

	Route::resource('/groups', 'Group\AdminGroupController');
	Route::get('/groups_json_paginate', 'Group\AdminGroupController@groups');
	Route::get('/groups_json_parent', 'Group\AdminSubGroupController@parentGroups');
	Route::get('/child_groups_json', 'Group\AdminSubGroupController@childGroup');
	Route::resource('/sub_groups', 'Group\AdminSubGroupController');

	Route::resource('/emergency-service','Emergency\ContactController')->except('show','edit');
	Route::post('/emergency-contact/store','Emergency\ContactController@contactStore')->name('emergency.contact.store');
	Route::get('/emergency/{service}/contact','Emergency\ContactController@allContact')->name('emergency.contact.all');
	Route::get('/training/user_search', 'Training\MamberController@allUserSearch');
	Route::resource('/training', 'Training\TrainingController');
	Route::resource('/training/{training}/members', 'Training\MamberController');
});

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/admin/login', 'AdminLoginController@showLoginForm')->name('admin.login');
// Route::post('/admin/login', 'AdminLoginController@login')->name('admin.login.submit');
// Route::get('/admin', 'AdminController@index')->name('admin.dashboard');
// Route::get('/users', 'UserController@all');
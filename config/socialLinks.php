<?php 

return [
	'links' => [
		1 => [
			'name' => 'faceBook',
			'icon' => 'fa-facebook',
			'color' => '3B5998',
		],
		2 => [
			'name' => 'twitter',
			'icon' => 'fa-twitter',
			'color' => '00aced',
		],
		3 => [
			'name' => 'google',
			'icon' => 'fa-google',
			'color' => 'd62d20',
		],
	]
];
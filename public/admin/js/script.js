/*
SIDE MENU :
-----------------------------------------------------*/
$(document).ready(function(){
    $(".user").click(function(){
        $(".menu-list-1").css("display", "block");
        $("#userList").addClass("sub-active");
        $(".menu-list-2").css("display", "none");
        $(".menu-list-3").css("display", "none");
        $(".menu-list-4").css("display", "none");
        $(".menu-list-5").css("display", "none");
        $(".menu-list-6").css("display", "none");
        //$(".user a").addClass("active");
        $(".em-service a").removeClass("active");
        $(".training a").removeClass("active");
        $(".online-poll a").removeClass("active");
        $(".group a").removeClass("active");
        $(".advertise a").removeClass("active");
    });

    $(".em-service").click(function(){
        $(".menu-list-2").css("display", "block");
        $(".menu-list-1").css("display", "none");
        $(".menu-list-3").css("display", "none");
        $(".menu-list-4").css("display", "none");
        $(".menu-list-5").css("display", "none");
        $(".menu-list-6").css("display", "none");
        $(".em-service a").addClass("active");
        $(".user a").removeClass("active");
        $(".training a").removeClass("active");
        $(".online-poll a").removeClass("active");
        $(".group a").removeClass("active");
        $(".advertise a").removeClass("active");
    });

    $(".training").click(function(){
        $(".menu-list-3").css("display", "block");
        $(".menu-list-1").css("display", "none");
        $(".menu-list-2").css("display", "none");
        $(".menu-list-4").css("display", "none");
        $(".menu-list-5").css("display", "none");
        $(".menu-list-6").css("display", "none");
        $(".training a").addClass("active");
        $(".user a").removeClass("active");
        $(".em-service a").removeClass("active");
        $(".online-poll a").removeClass("active");
        $(".group a").removeClass("active");
        $(".advertise a").removeClass("active");
    });

    $(".online-poll").click(function(){
        $(".menu-list-4").css("display", "block");
        $(".menu-list-1").css("display", "none");
        $(".menu-list-2").css("display", "none");
        $(".menu-list-3").css("display", "none");
        $(".menu-list-5").css("display", "none");
        $(".menu-list-6").css("display", "none");
        $(".online-poll a").addClass("active");
        $(".user a").removeClass("active");
        $(".em-service a").removeClass("active");
        $(".training a").removeClass("active");
        $(".group a").removeClass("active");
        $(".advertise a").removeClass("active");
    });

    $(".group").click(function(){
        $(".menu-list-5").css("display", "block");
        $(".menu-list-1").css("display", "none");
        $(".menu-list-2").css("display", "none");
        $(".menu-list-3").css("display", "none");
        $(".menu-list-4").css("display", "none");
        $(".menu-list-6").css("display", "none");
        $(".group a").addClass("active");
        $(".user a").removeClass("active");
        $(".em-service a").removeClass("active");
        $(".training a").removeClass("active");
        $(".online-poll a").removeClass("active");
        $(".advertise a").removeClass("active");
    });

    $(".advertise").click(function(){
        $(".menu-list-6").css("display", "block");
        $(".menu-list-1").css("display", "none");
        $(".menu-list-2").css("display", "none");
        $(".menu-list-3").css("display", "none");
        $(".menu-list-4").css("display", "none");
        $(".menu-list-5").css("display", "none");
        $(".advertise a").addClass("active");
        $(".user a").removeClass("active");
        $(".em-service a").removeClass("active");
        $(".training a").removeClass("active");
        $(".online-poll a").removeClass("active");
        $(".group a").removeClass("active");
    });
});

/*
LOGIN PAGE :
-----------------------------------------------------*/
$(document).ready(function(){
    $(".forgot-pass").click(function(){
        $("#forgot-password").fadeIn();
        $("#login").fadeOut();
    });
});
$(document).ready(function(){
    $("#btn-forgot-submit").click(function(){
        $("#forgot-password").fadeOut();
        $("#new-password").fadeIn();
    });
});
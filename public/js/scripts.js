$(document).ready(function() {
  
$("#scroll-top").hide();
  // fade in #back-top
  $(function () {
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('#scroll-top').fadeIn();
      } else {
        $('#scroll-top').fadeOut();
      }
    });
    // scroll body to 0px on click
    $('#scroll-top button').click(function () {
      $('body,html').animate({
        scrollTop: 0
      }, 800);
    });
  });

$('.imageGallery1 a').simpleLightbox();
$('.imageGallery .popupimagesnow').simpleLightbox();


var click = 0;
$(".password-hidden-hide i").click(function(){
	++click;
	if(click%2 === 0) {
		$(".password-hidden-hide i").removeClass("fa-eye-slash");
		$(".password-hidden-hide i").addClass("fa-eye");
	} else {
		$(".password-hidden-hide i").removeClass("fa-eye");
		$(".password-hidden-hide i").addClass("fa-eye-slash");
	}
});


$(".password-hidden-hide i").click(function(){
	$(".password-hidden-hide").toggleClass("showpassword");
});

var postvideo=document.getElementById("post-video"); 

function playPause(){ 
  if (postvideo.paused) 
    postvideo.play(); 
  else 
    postvideo.pause(); 
  }
});
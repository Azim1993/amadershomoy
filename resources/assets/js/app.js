
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VModal from 'vue-js-modal'
import VeeValidate from 'vee-validate'

Vue.use(VeeValidate)
Vue.use(VModal)

export const eventBusForSignUpBtn = new Vue({
  methods: {
    changeComponentChild (eventComponent) {
      this.$emit('changeComponentParent', eventComponent)
    }
  }
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
/*==> Register and login conponent <==*/
Vue.component('login', require('./components/user/auth/Login.vue'));
Vue.component('register', require('./components/user/auth/Register.vue'));

Vue.component('profile-avater', require('./components/user/profile/Avater.vue'));
Vue.component('profile-cover', require('./components/user/profile/Cover.vue'));
Vue.component('user-leftbar', require('./components/user/profile/LeftBar.vue'));
Vue.component('basic-infobar', require('./components/user/anononus/BasicInfobar.vue'));

Vue.component('general', require('./components/user/profile/General.vue'));
Vue.component('general-popup', require('./components/user/popup/GeneralUp.vue'));

Vue.component('work', require('./components/user/profile/Work.vue'));
Vue.component('work-popup', require('./components/user/popup/WorkUp.vue'));

Vue.component('education-popup', require('./components/user/popup/EducationUp.vue'));
Vue.component('education', require('./components/user/profile/Education.vue'));

Vue.component('contact', require('./components/user/profile/Contact.vue'));
Vue.component('contact-popup', require('./components/user/popup/ContactUp.vue'));
Vue.component('social-popup', require('./components/user/popup/SocialUp.vue'));
Vue.component('social-links', require('./components/user/profile/SocialLinks.vue'));

Vue.component('awards', require('./components/user/profile/Awards.vue'));
Vue.component('awards-popup', require('./components/user/popup/AwardsUp.vue'));

Vue.component('networks', require('./components/user/network/Index.vue'));

Vue.component('post-create', require('./components/post/PostCreate.vue'));
Vue.component('post-show', require('./components/post/PostShow.vue'));
Vue.component('post-news', require('./components/post/news/News.vue'));

Vue.component('user-groups', require('./components/user/group/Index.vue'));
Vue.component('user-group', require('./components/user/group/SingleGroup.vue'));
//=============================================
// Admin components
// ============================================
Vue.component('group-index', require('./components/admin/group/Index.vue'));
Vue.component('sub-group-index', require('./components/admin/group/SubIndex.vue'));
Vue.component('group-select', require('./components/admin/group/GroupSelect.vue'));
Vue.component('admin-user-search', require('./components/admin/UserSearch.vue'));
const app = new Vue({
    el: '#app',
    data: {
    	visible: false,
        modalIsOpen: false
    },
    methods: {
        showModal: function() {
            this.modalIsOpen = true;
        }
    }
});

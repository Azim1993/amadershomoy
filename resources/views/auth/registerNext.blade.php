@extends('layouts.app')

@section('content')
<section class="sing-up-area overflow-fix" style="background:url({{ asset('/images/guest/bg-sing.png')  }});">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="login-banner-area overflow-fix">
                        <a href=""><img src="{{ asset('/images/guest/user.png') }}"/></a>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="login-form-filed-area overflow-fix">
                        <div class="login-form-filed-title overflow-fix">
                            <h2>
                                Must fill up<br/>following information.
                            </h2>
                            <p>Provide your valid information.</p>
                        </div>
                        <div class="login-form-filed-select two-s overflow-fix">
                            <div class="single-select-title">
                                <p>Gender</p>
                                <div class="single-select-title-icon">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    <select class="post-list-me">
                                        <option>Male</option>
                                        <option>FeMale</option>
                                    </select>
                                </div>
                                
                            </div>
                            <div class="single-select-title">
                                <p>Blood Group</p>
                                <div class="single-select-title-icon">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    <select class="post-list-me">
                                        <option>A+aaaaaa</option>
                                        <option>-A</option>
                                    </select>
                                </div>
                            
                            </div>
                        </div>
                        <div class="login-form-filed-select one-s overflow-fix">
                            <div class="single-select-title">
                                <p>geographical area</p>
                                <div class="row">
                                    <div class="col-sm-6 form-group single-select-title-icon">
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        <select class="post-list-me">
                                            <option value="">select Division</option>
                                            <option>Dhanmondi, Dhaka, Bangladesh,</option>
                                            <option>Dhan, Dhaka, Bangladesh,</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6 form-group single-select-title-icon">
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        <select class="post-list-me">
                                            <option value="">Select District</option>
                                            <option>Dhanmondi, Dhaka, Bangladesh,</option>
                                            <option>Dhan, Dhaka, Bangladesh,</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-12 form-group single-select-title-icon">
                                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        <textarea class="form-control" name="address" placeholder="Address Detail"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="login-form-filed-select one-s overflow-fix">
                            <div class="single-select-title">
                                <p>DATE OF BIRTH</p>
                                <div class="single-select-title-icon">
                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                    <input type="date" placeholder="Birth Date "/>
                                </div>
                            </div>
                        </div>
                        <div class="login-form-filed-select one-s overflow-fix">
                            <div class="single-select-title">
                                <p>Study</p>
                                <div class="single-select-title-icon">
                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                    <input placeholder="University of Dhaka "/>
                                </div>
                            </div>
                        </div>
                        <div class="login-form-filed-select one-s overflow-fix">
                            <div class="single-select-title">
                                <p>Mobile Number</p>
                                <div class="single-select-title-icon">
                                    <i class="fa fa-volume-control-phone" aria-hidden="true"></i>
                                    <input placeholder="+0088 01755-947837"/>
                                </div>
                                
                            </div>
                        </div>
                        <div class="login-form-filed-select one-s overflow-fix">
                            <div class="single-select-title">
                                <p>Profession</p>
                                <div class="single-select-title-icon">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    <select class="post-list-me">
                                        <option>Journalist</option>
                                        <option>Profession</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary contunio-buton-for">
                            sign up
                        </button>
                    </div>
                </div>
                
            </div>
        </div>
</section>
@endsection

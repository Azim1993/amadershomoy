@extends('layouts.app')

@section('content')
<section class="sing-up-area overflow-fix" style="background:url({{ asset('/images/guest/bg-sing.png')  }});">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="login-banner-area overflow-fix">
                        <a href=""><img src="{{ asset('/images/guest/user.png') }}"/></a>
                    </div>
                </div>
                <div class="col-lg-5">
                    <register></register>
                </div>
            </div>
        </div>
</section>
@endsection

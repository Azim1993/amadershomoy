@extends('layouts.aapp')

@section('title')
	home
@endsection

@section('css')

@endsection

@section('content')
<!--==================================
CONTENT
===================================-->
<!--==================================
LOGIN
===================================-->
<section id="login">
	<div class="col-sm-4 col-sm-offset-4">
		<!-- LOGIN -->
		<form class="login" action="{{ route('admin.login.store') }}" method="post">
			{{ csrf_field() }}
			<div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
			    <label>Username</label>
			    <input id="form_user_id" type="text" 
			    	name="email" class="form-control" 
			    	required="required" placeholder="Username"
			    	value="{{ old('email') }}" required autofocus>
			    @include('include.formErrorText', ['inputName' => 'email'])
			</div>		            
			<div class="form-group {{ $errors->has('password')? 'has-error':'' }}">
			    <label>Password</label>
			    <input id="form_pass" type="password"
			    	name="password" class="form-control"
			    	placeholder="Password" required>
			    @include('include.formErrorText', ['inputName' => 'password'])
			</div>
			<div class="clearfix">
				<button type="submit" class="btn-login">Login</button>
			</div>
			<span class="checkbox">
				<label>
					<input type="checkbox" name="remember" 
					{{ old('remember') ? 'checked' : '' }}> 
					Remember me
				</label>	
			</sapn>
			<span class="forgot-pass"><label>Forgot Password?</label></span>			
		</form>
		<!-- /LOGIN -->
	</div>
</section>
<!--==================================
/LOGIN
===================================-->
<!--==================================
/CONTENT
===================================-->

@endsection

@section('js')

@endsection
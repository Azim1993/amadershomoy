<div class="col-lg-8">
	<div class="single-about-area overflow-fix">
		<div class="single-about-title-area overflow-fix ">
			<p>General</p>
			<span><i class="fa fa-pencil" @click="$modal.show('general-popup')" aria-hidden="true"></i></span>
		</div>
		<general></general>
		<general-popup></general-popup>
	</div>
	<div class="single-about-area hover-and-removed overflow-fix">
		<div class="single-about-title-area overflow-fix ">
			<p>Work</p>
			<span>
				<i class="fa fa-pencil" aria-hidden="true"></i>
				<i  @click="$modal.show('work-popup')" class="fa fa-plus" aria-hidden="true"></i>
			</span>
		</div>
		<work></work>
		<work-popup></work-popup>
	</div>
	<div class="single-about-area hover-and-removed overflow-fix">
		<div class="single-about-title-area overflow-fix ">
			<p>Education</p>
			<span>
				<i class="fa fa-pencil" aria-hidden="true"></i>
				<i  @click="$modal.show('education-popup')"  class="fa fa-plus" aria-hidden="true"></i>
			</span>
		</div>
		<education></education>
		<education-popup></education-popup>
	</div>
	<div class="single-about-area overflow-fix">
		<div class="single-about-title-area overflow-fix ">
			<p>Contact</p>
			<span><i @click="$modal.show('contact-popup')" class="fa fa-pencil" aria-hidden="true"></i></span>
		</div>
		<contact></contact>
		<contact-popup></contact-popup>
	</div>
</div>
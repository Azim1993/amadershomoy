<div class="col-lg-4">
	<div class="about-sidebar-area overflow-fix">
		<div class="award-sidebar-area overflow-fix">
			<div class="award-sidebar-title-area overflow-fix">
				<h2>Award</h2>
				<span>
					<i @click="$modal.show('award-popup')" class="fa fa-plus" aria-hidden="true"></i>
				</span>
			</div>
			<awards></awards>
			<awards-popup></awards-popup>
		</div>
		<div class="award-sidebar-area overflow-fix">
			<div class="award-sidebar-title-area overflow-fix">
				<h2>Certificate</h2>
			</div>
			<div class="award-sidebar-content-area overflow-fix">
				<h2>Journalism in Agriculture<span>2016</span></h2>
				<img src="{{ asset('images/cerfied-1.png') }}"/>
			</div>
		</div>
		<div class="award-sidebar-area overflow-fix">
			<div class="single-about-title-area overflow-fix ">
				<p>On Social</p>
				<span>
					<i class="fa fa-pencil" aria-hidden="true"></i>
					<i @click="$modal.show('social-popup')" class="fa fa-plus" aria-hidden="true"></i>
				</span>
			</div>
			<social-links></social-links>
			<social-popup></social-popup>
		</div>
	</div>
</div>
@extends('layouts.uapp')

@section('content')

@include('user.includes.topProfile')
<section class="main-about-area overflow-fix">
	<div class="container my-container">
		<div class="row">
			@include('user.profile.content')
			@include('user.profile.rightBar')
		</div>
	</div>
</section>


@endsection
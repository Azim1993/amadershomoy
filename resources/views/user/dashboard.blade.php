@extends('layouts.uapp')

@section('content')
<section class="front-page overflow-fix">
    <div class="container my-container">
        <div class="row">
            <div class="col-lg-3">
                @include('user.includes.leftBar')
            </div>
            <section class="col-lg-6">
                <div class="main-content-area overflow-fix">
                    <div class="whats-your-mind-area overflow-fix">
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="whats-your-mind-profile-picurt-area overflow-fix">
                                    <a href=""><img src="images/profile.png"/></a>
                                </div>
                            </div>
                            <post-create></post-create>
                        </div>
                    </div>

                    <post-news></post-news>
                    <post-show v-for="n in 20" :key="n"></post-show>
                </div>
            </section>
            <aside class="col-lg-3">
                @include('user.includes.profileRight')
            </aside>
        </div>
    </div>
</section>

@endsection

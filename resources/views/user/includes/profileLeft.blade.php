<div class="left-sidebar-area overflow-fix">
    <div class="user-profile-area rate-this-person-area overflow-fix">
        <h2 class="wid-get-title">Rate this person</h2>
        <div class="user-profile-ratiing-area overflow-fix">    
            <ul>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
            </ul>
        </div>
    </div>
    <div class="user-profile-post-flow-floing-area overflow-fix background-white d-flex justify-content-between">
        <div class="user-profile-post-flow-floing-single-area overflow-fix">
            <p>Followers</p>
            <span>169</span>
        </div>
        <div class="user-profile-post-flow-floing-single-area overflow-fix">
            <p>Following</p>
            <span>25</span>
        </div>
    </div>
    <div class="user-profile-social-area overflow-fix background-white">    
        <h2 class="wid-get-title">On the social</h2>
        <ul>
            <li><a href=""><i class="fa fa-google-plus-official" aria-hidden="true"></i></a></li>
            <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
            <li><a href=""><i class="fa fa-android" aria-hidden="true"></i></a></li>
            <li><a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        </ul>
    </div>
    <div class="user-profile-awards-area overflow-fix background-white">    
        <h2 class="wid-get-title">Awards</h2>
        <ul class="d-flex align-items-left">
            <li><a href=""><img src="images/ward-1.png"/></a></li>
            <li><a href=""><img src="images/ward-2.png"/></a></li>
        </ul>
    </div>
    <div class="user-profile-certificate-area overflow-fix background-white">   
        <h2 class="wid-get-title">Certificate</h2>
        <p class="error-color-text">You have not received any Certificate yet.</p>
    </div>
    <div class="user-profile-member-since-area overflow-fix background-white">  
        <p>Member Since:<span> 26 July, 2017</span></p>
    </div>

    <div class="user-profile-member-group-area overflow-fix background-white">  
        <div class="user-profile-member-group-title overflow-fix">
            <a href="" class="d-flex"><img src="images/group.png"/><p>Group</p></a>
        </div>
        <div class="user-profile-member-group-list overflow-fix">
            <ul>
                <li><a href=""><span>Dhaka</span>-<span>1500 members</span></a></li>
                <li><a href=""><span>Khulna</span>-<span>1200 members</span></a></li>
                <li><a href=""><span>Chittagong</span>-<span>1300 members</span></a></li>
                <li><a href=""><span>Syllhet</span>-<span>900 members</span></a></li>
            </ul>
        </div>
    </div>
    <div class="sidebar-copy-right-area overflow-fix">
        <ul>
            <li><a href="">Privacy Policy -</a></li>
            <li><a href="">Terms & Conditions -</a></li>
            <li><a href="">Help Center</a></li>
            <li><a href="">Amader Shomoy Social &copy 2017</a></li>
        </ul>
    </div>
    </div>
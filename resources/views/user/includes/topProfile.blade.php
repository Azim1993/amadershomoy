<section class="my-profile-header-area overflow-fix">
    <div class="container my-container">
        <div class="row">
            <div class="col-lg-12">
                <div class="my-profile-header-cover-image overflow-fix">
                    <profile-cover></profile-cover>
                    <profile-avater></profile-avater>
                </div>
                <div class="my-profile-header-cover-menu overflow-fix">
                    <div class="row margin-o">
                        <div class="col-lg-2 padding-o">
                            
                        </div>
                        <div class="col-lg-3">
                            <div class="my-profile-header-cover-profile-text overflow-fix">
                                <p class="my-profile-header-cover-profile-top">Shykh Seraj</p>
                                <p class="my-profile-header-cover-profile-text-bottom">
                                    Director  & Head of News<br/><span>Impress Telefilm Ltd.</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="my-profile-header-cover-profile-menu-list overflow-fix">
                                <ul>    
                                    <li><a class="{{ Request::is('/')?'active':'' }}" href="{{ url('/') }}">Post<span>18</span></a></li>
                                    <li><a class="{{ Request::is('profile')?'active':'' }}" href="{{ route('user.profile') }}">about<span></span></a></li>
                                    <li><a href="">photos<span></span></a></li>
                                    <li><a href="">video<span></span></a></li>
                                    <li><a href="">audios<span></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
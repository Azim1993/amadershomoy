@extends('layouts.uapp')

@section('content')
<section class="front-page overflow-fix">
    <div class="container my-container">
        <div class="row">
            <div class="col-lg-3">
                @include('user.includes.leftBar')
            </div>
            <section class="col-lg-6">
            <div class="main-folo-oarea overflow-fix">
               <user-groups></user-groups>
            </div>
            </section>
            <aside class="col-lg-3">
                @include('user.includes.profileRight')
            </aside>
        </div>
    </div>
</section>

@endsection

@extends('layouts.uapp')

@section('content')
<section class="front-page overflow-fix">
    <div class="container my-container">
        <div class="row">
            <div class="col-lg-3">
                @include('user.includes.leftBar')
            </div>
            <section class="col-lg-9">
                <user-group :group-id="{{ json_encode($group) }}"></user-group>
            </section>
        </div>
    </div>
</section>

@endsection

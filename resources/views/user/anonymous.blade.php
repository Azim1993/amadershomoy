@extends('layouts.uapp')

@section('content')
<section class="front-page overflow-fix">
    <div class="container my-container">
        <div class="row">
            <div class="col-lg-3">
                @include('user.includes.annoLeftBar')
            </div>
            <section class="col-lg-6">
                <div class="main-content-area overflow-fix">
                    <div class="announces-bar-page-link overflow-fix">
                        <ul>
                            <li><a class="active" href="#">Post</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Video</a></li>
                            <li><a href="#">Audio</a></li>
                        </ul>
                        <ul>
                            <li>
                                <span>
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <input type="text" id="datepicker">
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div class="whats-your-mind-single-post overflow-fix">
                        <ul id="my-new-menu">
                          <li>
                            <input class="my-new-menu-input" id="menu1" type="checkbox" name="menu"/>
                            <label class="my-new-menu-label" for="menu1"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></label>
                            <ul class="my-new-menu-submenu">
                              <li><a href="#">Edit</a></li>
                              <li><a href="#">Delete</a></li>
                            </ul>
                          </li>
                        </ul>
                        <div class="whats-your-mind-single-post-user-img-area overflow-fix">    
                            <div class="whats-your-mind-single-post-user-img overflow-fix"> 
                                <a href=""><img src="/images/profile.png"/></a>
                            </div>
                            <div class="whats-your-mind-single-post-user-img-right-content overflow-fix">   
                                <h2>Utpal Shuvro<span>4.0<span><i class="fa fa-star-o" aria-hidden="true"></i></span></span></h2>
                                <p class="d-flex">Sports Editor at <span><a href="">The Daily Prothom Alo</a></span>   -   <span>1hr</span></p>
                            </div>
                        </div>
                        <div class="whats-your-mind-single-post-content-area overflow-fix"> 
                            ইংল্যান্ডের জার্সিতে আর দেখা যাবে না ইংলিশ ফরোয়ার্ড ওয়েইন রুনিকে। প্রায় দেড় দশক ধরে জাতীয় দলের হয়ে প্রতিনিধি ১০ নম্বর জার্সিতে নিজেকে দারুণভাবে মেলে ধরা রুনি আন্তর্জাতিক ফুটবলকে বিদায় জানিয়েছেন।
                            তবে ৩১ বছর বয়সী এই ফুটবলার ক্লাবের হয়ে খেলা চালিয়ে যাবেন। বর্তমানে খেলছেন এভারটনের হয়ে। এই ক্লাবেই তার সিনিয়র ক্যারিয়ারের যাত্রা শুরু হয়েছিল। এছাড়া ম্যানচেস্টার ইউনাউটেডও তাকে মনে রাখবে।
                            <div class="collapse search-bar-header" id="xpand-content">
                                ইংল্যান্ডের জার্সিতে আর দেখা যাবে না ইংলিশ ফরোয়ার্ড ওয়েইন রুনিকে। প্রায় দেড় দশক ধরে জাতীয় দলের হয়ে প্রতিনিধি ১০ নম্বর জার্সিতে নিজেকে দারুণভাবে মেলে ধরা রুনি আন্তর্জাতিক ফুটবলকে বিদায় জানিয়েছেন।
                            তবে ৩১ বছর বয়সী এই ফুটবলার ক্লাবের হয়ে খেলা চালিয়ে যাবেন। বর্তমানে খেলছেন এভারটনের হয়ে। এই ক্লাবেই তার সিনিয়র ক্যারিয়ারের যাত্রা শুরু হয়েছিল। এছাড়া ম্যানচেস্টার ইউনাউটেডও তাকে মনে রাখবে।
                            </div>
                            <a class="collapsed" data-toggle="collapse" href="#xpand-content" aria-expanded="true" aria-controls="xpand-content"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>
                        </div>
                        <div class="whats-your-mind-single-post-content-count overflow-fix">    
                            <ul class="d-flex">
                                <li>
                                    <a href="">2 Likes</a>
                                </li>
                                <li>
                                    <a href="">5 Comments</a>
                                </li>
                                <li>
                                    <a href="">1 Share</a>
                                </li>
                            </ul>
                        </div>
                        <div class="whats-your-mind-single-post-content-count-me overflow-fix"> 
                            <ul class="d-flex">
                                <li>
                                    <a href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>like</a>
                                </li>
                                <li>
                                    <a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i>Comments</a>
                                </li>
                                <li>
                                    <a href=""><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="news-ads-area overflow-fix">
                        <div class="single-neus-area-home overflow-fix">
                            <a href=""><img src="/images/news-1.png"/></a>
                            <h2>ইংল্যান্ডের জার্সিতে আর দেখা যাবে না ইংলিশ</h2>
                            <p>০৬ ডিসেম্বর, ২০১৭</p>
                        </div>
                        <div class="single-neus-area-home overflow-fix">
                            <a href=""><img src="/images/news-3.png"/></a>
                            <h2>ইংল্যান্ডের জার্সিতে আর দেখা যাবে না ইংলিশ</h2>
                            <p>০৬ ডিসেম্বর, ২০১৭</p>
                        </div>
                        <div class="single-neus-area-home overflow-fix">
                            <a href=""><img src="/images/news-2.png"/></a>
                            <h2>ইংল্যান্ডের জার্সিতে আর দেখা যাবে না ইংলিশ</h2>
                            <p>০৬ ডিসেম্বর, ২০১৭</p>
                        </div>
                        <div class="single-neus-area-home overflow-fix">
                            <a href=""><img src="/images/news-1.png"/></a>
                            <h2>ইংল্যান্ডের জার্সিতে আর দেখা যাবে না ইংলিশ</h2>
                            <p>০৬ ডিসেম্বর, ২০১৭</p>
                        </div>
                    </div>
                    <div class="whats-your-mind-single-post overflow-fix">
                        <ul id="my-new-menu">
                          <li>
                            <input class="my-new-menu-input" id="menu2" type="checkbox" name="menu"/>
                            <label class="my-new-menu-label" for="menu2"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></label>
                            <ul class="my-new-menu-submenu">
                              <li><a href="#">Edit</a></li>
                              <li><a href="#">Delete</a></li>
                            </ul>
                          </li>
                        </ul>
                        <div class="whats-your-mind-single-post-user-img-area overflow-fix">    
                            <div class="whats-your-mind-single-post-user-img overflow-fix"> 
                                <a href=""><img src="/images/post-1.png"/></a>
                            </div>
                            <div class="whats-your-mind-single-post-user-img-right-content overflow-fix">   
                                <h2>Utpal Shuvro<span>4.0<span><i class="fa fa-star-o" aria-hidden="true"></i></span></span></h2>
                                <p class="d-flex">Sports Editor at <span><a href="">The Daily Prothom Alo</a></span>   -   <span>1hr</span></p>
                            </div>
                        </div>
                        <div class="whats-your-mind-single-post-content-area overflow-fix"> 
                            <a href=""><img src="/images/post-1.png"/></a>
                        </div>
                        <div class="whats-your-mind-single-post-content-count overflow-fix">    
                            <ul class="d-flex">
                                <li>
                                    <a href="">2 Likes</a>
                                </li>
                                <li>
                                    <a href="">5 Comments</a>
                                </li>
                                <li>
                                    <a href="">1 Share</a>
                                </li>
                            </ul>
                        </div>
                        <div class="whats-your-mind-single-post-content-count-me overflow-fix"> 
                            <ul class="d-flex">
                                <li>
                                    <a href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>like</a>
                                </li>
                                <li>
                                    <a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i>Comments</a>
                                </li>
                                <li>
                                    <a href=""><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a>
                                </li>
                            </ul>
                        </div>
                        <div class="box comment-box-area overflow-fix">
                            <a href=""><p class="prev-comment">Show previous comments</p></a>
                            <div class="media">
                              <img class="mr-3" src="/images/profile.png" alt="Generic placeholder image">
                              <div class="media-body">
                                <div class="comment-text-prover">
                                    <a class="comment-profile-name" href=""><h5 class="mt-0">Munna Kazi</h5></a>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                </div>
                                <div class="comment-count-box d-flex">
                                    <ul class="d-flex">
                                        <li>
                                            <a href="">Likes</a>
                                        </li>
                                        <li>
                                            <a href="">Reply</a>
                                        </li>
                                    </ul>
                                    <ul class="d-flex">
                                        <li>
                                            <a href="">2 Likes</a>
                                        </li>
                                        <li>
                                            <a href="">5 Reply</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="comment-replay-box-2 overflow-fix">
                                    <div class="media mt-3">
                                      <a class="pr-3" href="#">
                                        <img src="/images/profile.png" alt="Generic placeholder image">
                                      </a>
                                      <div class="media-body">
                                        <div class="comment-text-prover">
                                            <a class="comment-profile-name" href=""><h5 class="mt-0">Munna Kazi</h5></a>
                                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                        </div>
                                        <div class="comment-count-box d-flex overflow-fix">
                                            <ul class="d-flex">
                                                <li>
                                                    <a href="">Likes</a>
                                                </li>
                                                <li>
                                                    <a href="">Reply</a>
                                                </li>
                                            </ul>
                                            <ul class="d-flex">
                                                <li>
                                                    <a href="">2 Likes</a>
                                                </li>
                                                <li>
                                                    <a href="">5 Reply</a>
                                                </li>
                                            </ul>
                                        </div>
                                      </div>
                                    </div>
                                    
                                </div>
                                <div class="comment-replay-box-2 overflow-fix">
                                    <div class="media mt-3">
                                      <a class="pr-3" href="#">
                                        <img src="/images/profile.png" alt="Generic placeholder image">
                                      </a>
                                      <div class="media-body">
                                        <div class="comment-text-prover">
                                            <a class="comment-profile-name" href=""><h5 class="mt-0">Munna Kazi</h5></a>
                                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                        </div>
                                        <div class="comment-count-box d-flex overflow-fix">
                                            <ul class="d-flex">
                                                <li>
                                                    <a href="">Likes</a>
                                                </li>
                                                <li>
                                                    <a href="">Reply</a>
                                                </li>
                                            </ul>
                                            <ul class="d-flex">
                                                <li>
                                                    <a href="">2 Likes</a>
                                                </li>
                                                <li>
                                                    <a href="">5 Reply</a>
                                                </li>
                                            </ul>
                                        </div>
                                      </div>
                                    </div>
                                    
                                </div>
                              </div>
                            </div>
                            <div class="media">
                              <img class="mr-3" src="/images/profile.png" alt="Generic placeholder image">
                              <div class="media-body">
                                <div class="comment-text-prover">
                                    <a class="comment-profile-name" href=""><h5 class="mt-0">Munna Kazi</h5></a>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                </div>
                                <div class="comment-count-box d-flex">
                                    <ul class="d-flex">
                                        <li>
                                            <a href="">Likes</a>
                                        </li>
                                        <li>
                                            <a href="">Reply</a>
                                        </li>
                                    </ul>
                                    <ul class="d-flex">
                                        <li>
                                            <a href="">2 Likes</a>
                                        </li>
                                        <li>
                                            <a href="">5 Reply</a>
                                        </li>
                                    </ul>
                                </div>
                              </div>
                            </div>
                            <div class="comment-input-field overflow-fix">
                                <div class="comment-input-field-profile-img overflow-fix">
                                    <a href=""><img src="/images/profile.png" alt="Generic placeholder image"></a>
                                </div>
                                <div class="comment-input-field-profile-input overflow-fix">
                                    <input type="text"/>
                                        <div class="profile-img-upload-area js--image-preview overflow-fix" style="background-image: url(images/profile-pic.png);">
                                            
                                        </div>
                                        <div class="profile-img-upload-button overflow-fix">
                                            
                                            <span><i class="fa fa-camera" aria-hidden="true"></i><input type="file" class="image-upload" name="Drag or Upload Files" accept="media_type/*"/></span> 
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="whats-your-mind-single-post overflow-fix">
                        <ul id="my-new-menu">
                          <li>
                            <input class="my-new-menu-input" id="menu3" type="checkbox" name="menu"/>
                            <label class="my-new-menu-label" for="menu3"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></label>
                            <ul class="my-new-menu-submenu">
                              <li><a href="#">Edit</a></li>
                              <li><a href="#">Delete</a></li>
                            </ul>
                          </li>
                        </ul>
                        <div class="whats-your-mind-single-post-user-img-area overflow-fix">    
                            <div class="whats-your-mind-single-post-user-img overflow-fix"> 
                                <a href=""><img src="/images/post-1.png"/></a>
                            </div>
                            <div class="whats-your-mind-single-post-user-img-right-content overflow-fix">   
                                <h2>Utpal Shuvro<span>4.0<span><i class="fa fa-star-o" aria-hidden="true"></i></span></span></h2>
                                <p class="d-flex">Sports Editor at <span><a href="">The Daily Prothom Alo</a></span>   -   <span>1hr</span></p>
                            </div>
                        </div>
                        <div class="whats-your-mind-single-post-content-area overflow-fix"> 
                            <p>ইংল্যান্ডের জার্সিতে আর দেখা যাবে না ইংলিশ ফরোয়ার্ড ওয়েইন রুনিকে।</p>
                            <div class="imageGallery1 video-upload-post">
                                <img src="/images/post-1.png" alt="Gallery image 1" />
                                <a href="https://www.youtube.com/embed/qtQgbdmIO30?autoplay=true"><i class="fa fa-play-circle-o" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="whats-your-mind-single-post-content-count overflow-fix">    
                            <ul class="d-flex">
                                <li>
                                    <a href="">2 Likes</a>
                                </li>
                                <li>
                                    <a href="">5 Comments</a>
                                </li>
                                <li>
                                    <a href="">1 Share</a>
                                </li>
                            </ul>
                        </div>
                        <div class="whats-your-mind-single-post-content-count-me overflow-fix"> 
                            <ul class="d-flex">
                                <li>
                                    <a href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>like</a>
                                </li>
                                <li>
                                    <a href=""><i class="fa fa-commenting-o" aria-hidden="true"></i>Comments</a>
                                </li>
                                <li>
                                    <a href=""><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a>
                                </li>
                            </ul>
                        </div>
                        <div class="box comment-box-area overflow-fix">
                            <a href=""><p class="prev-comment">Show previous comments</p></a>
                            <div class="media">
                              <img class="mr-3" src="/images/profile.png" alt="Generic placeholder image">
                              <div class="media-body">
                                <div class="comment-text-prover">
                                    <a class="comment-profile-name" href=""><h5 class="mt-0">Munna Kazi</h5></a>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                </div>
                                <div class="comment-count-box d-flex">
                                    <ul class="d-flex">
                                        <li>
                                            <a href="">Likes</a>
                                        </li>
                                        <li>
                                            <a href="">Reply</a>
                                        </li>
                                    </ul>
                                    <ul class="d-flex">
                                        <li>
                                            <a href="">2 Likes</a>
                                        </li>
                                        <li>
                                            <a href="">5 Reply</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="comment-replay-box-2 overflow-fix">
                                    <div class="media mt-3">
                                      <a class="pr-3" href="#">
                                        <img src="/images/profile.png" alt="Generic placeholder image">
                                      </a>
                                      <div class="media-body">
                                        <div class="comment-text-prover">
                                            <a class="comment-profile-name" href=""><h5 class="mt-0">Munna Kazi</h5></a>
                                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                        </div>
                                        <div class="comment-count-box d-flex overflow-fix">
                                            <ul class="d-flex">
                                                <li>
                                                    <a href="">Likes</a>
                                                </li>
                                                <li>
                                                    <a href="">Reply</a>
                                                </li>
                                            </ul>
                                            <ul class="d-flex">
                                                <li>
                                                    <a href="">2 Likes</a>
                                                </li>
                                                <li>
                                                    <a href="">5 Reply</a>
                                                </li>
                                            </ul>
                                        </div>
                                      </div>
                                    </div>
                                    
                                </div>
                                <div class="comment-replay-box-2 overflow-fix">
                                    <div class="media mt-3">
                                      <a class="pr-3" href="#">
                                        <img src="/images/profile.png" alt="Generic placeholder image">
                                      </a>
                                      <div class="media-body">
                                        <div class="comment-text-prover">
                                            <a class="comment-profile-name" href=""><h5 class="mt-0">Munna Kazi</h5></a>
                                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                        </div>
                                        <div class="comment-count-box d-flex overflow-fix">
                                            <ul class="d-flex">
                                                <li>
                                                    <a href="">Likes</a>
                                                </li>
                                                <li>
                                                    <a href="">Reply</a>
                                                </li>
                                            </ul>
                                            <ul class="d-flex">
                                                <li>
                                                    <a href="">2 Likes</a>
                                                </li>
                                                <li>
                                                    <a href="">5 Reply</a>
                                                </li>
                                            </ul>
                                        </div>
                                      </div>
                                    </div>
                                    
                                </div>
                              </div>
                            </div>
                            <div class="media">
                              <img class="mr-3" src="/images/profile.png" alt="Generic placeholder image">
                              <div class="media-body">
                                <div class="comment-text-prover">
                                    <a class="comment-profile-name" href=""><h5 class="mt-0">Munna Kazi</h5></a>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                                </div>
                                <div class="comment-count-box d-flex">
                                    <ul class="d-flex">
                                        <li>
                                            <a href="">Likes</a>
                                        </li>
                                        <li>
                                            <a href="">Reply</a>
                                        </li>
                                    </ul>
                                    <ul class="d-flex">
                                        <li>
                                            <a href="">2 Likes</a>
                                        </li>
                                        <li>
                                            <a href="">5 Reply</a>
                                        </li>
                                    </ul>
                                </div>
                              </div>
                            </div>
                            <div class="comment-input-field overflow-fix">
                                <div class="comment-input-field-profile-img overflow-fix">
                                    <a href=""><img src="/images/profile.png" alt="Generic placeholder image"></a>
                                </div>
                                <div class="comment-input-field-profile-input overflow-fix">
                                    <input type="text"/>
                                        <div class="profile-img-upload-area js--image-preview overflow-fix" style="background-image: url(/images/profile-pic.png);">
                                            
                                        </div>
                                        <div class="profile-img-upload-button overflow-fix">
                                            
                                            <span><i class="fa fa-camera" aria-hidden="true"></i><input type="file" class="image-upload" name="Drag or Upload Files" accept="media_type/*"/></span> 
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <aside class="col-lg-3">
                @include('user.includes.profileRight')
            </aside>
        </div>
    </div>
</section>

@endsection

@extends('layouts.aapp')
@inject('location', 'App\Repository\LocationRepo')

@section('title')
	home
@endsection

@section('css')

@endsection

@section('content')
	@include('include.flash')
	<!-- GROUP-LIST -->
	<div class="info-container-top margin-bottom">
		<div class="row">
			<div class="col-sm-8">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
			</div>
			<div class="col-sm-4 text-right">
				<div class="btn-area">
					
					<button data-toggle="modal" data-target="#serviceCreateModel" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i> ADD NEW Group</button>
				</div>
			</div>
		</div>
	</div>
	<div id="emergency-service" class="info-container">
	<!-- INFO-TABLE -->
	<div class="row">
	<div class="col-sm-8">
	<h4 class="text-primary">Emergency Services</h4><br>
	<div class="info-table table-responsive">
		@if($services->count() > 0)
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Service</th>
					<th>Collection</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@php $sl = 0 @endphp
				@foreach($services as $service)
				<tr>
					<td>{{ ++$sl }}</td>
					<td>{{ $service->name }}</td>
					<td>{{ $service->contacts->count() }}</td>
					<td>
						<a href="{{ route('emergency.contact.all',[$service]) }}" class="btn btn-primary btn-xs">All Contact</a>
						<a href="#" class="btn btn-success btn-xs">Edit</a>
						<a href="#" class="btn btn-danger btn-xs">Delete</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
			<div class="alert alert-warning">No Service Set Yet</div>
		@endif
	</div>
	</div>
	<div class="col-sm-4">
		<div class="panel panel-primary">
			<form action="{{ route('emergency.contact.store') }}" method="post">
				{{ csrf_field() }}
			<div class="panel-body">
				<h4 class="text-primary">Add Contact</h4><br>
				<div class="form-group {{ $errors->has('service_id')?'has-error':'' }}">
					<label for="servie"><b>Service</b></label>
					<select name="service_id" class="form-control">
						<option value="{{ null }}">Select Service</option>
						@if($services->count() > 0)
							@foreach($services as $service)
								<option value="{{ $service->id }}">{{ $service->name }}</option>
							@endforeach
						@endif
					</select>
					@include('include.formErrorText',['inputName' => 'service_id'])
				</div>
				<div class="form-group {{ $errors->has('district')?'has-error':'' }}">
					<label for="district"><b>District</b></label>
					<select id="district" name="district" class="form-control">
						<option value="{{ null }}">Select District</option>
						@if($location->getDistrict()->count() > 0)
							@foreach($location->getDistrict() as $district)
								<option value="{{ $district->id }}">{{ $district->name }}</option>
							@endforeach
						@endif
					</select>
					@include('include.formErrorText',['inputName' => 'district'])
				</div>
				<div class="form-group {{ $errors->has('upazila')?'has-error':'' }}">
					<label for="upazila"><b>Upazila</b></label>
					<select id="upazila" name="upazila" class="form-control">
						<option value="{{ null }}">Select Upazila</option>
						@if($location->getUpazila()->count() > 0)
							@foreach($location->getUpazila() as $upazila)
								<option data-type="{{$upazila->district_id}}" value="{{ $upazila->id }}">{{ $upazila->name }}</option>
							@endforeach
						@endif
					</select>
					@include('include.formErrorText',['inputName' => 'upazila'])
				</div>
				<div class="form-group {{ $errors->has('location')?'has-error':'' }}">
					<label for="location"><b>Location</b></label>
					<textarea name="location" class="form-control" placeholder="Enter Location"></textarea> 
					@include('include.formErrorText',['inputName' => 'location'])
				</div>
				<div class="form-group {{ $errors->has('contact_number')?'has-error':'' }}">
					<label for="contact_number"><b>Contact Number</b></label>
					<input name="contact_number" class="form-control" placeholder="Enter Location">
					@include('include.formErrorText',['inputName' => 'contact_number'])
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-cloud"></i> Upload Contact</button>
				</div>
			</div>
			</form>
		</div>
	</div>
	</div>
	<!-- /INFO-TABLE -->
	<!-- EM-CONTACT MODAL -->
	<div class="modal fade" id="serviceCreateModel" tabindex="-1" role="dialog" aria-labelledby="em-contactLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<h5 class="modal-title" id="warningModalLabel">Add New Service</h5>
				</div>
				<form method="post" action="{{ route('emergency-service.store') }}">
					{{ csrf_field() }}
				<div class="modal-body">
					<div class="form-group {{ $errors->has('name')?'has-error':'' }}">
						<label>Service Name</label>
						<input type="text" name="name" class="form-control" required="required" placeholder="Service Name">
						@include('include.formErrorText',['inputName' => 'name'])
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary btn-sm">
						<i class="fa fa-cloud"></i> Store </button>
				</div>
				</form>
			</div>
		</div>
	</div>
	</div>

@endsection

@section('js')
<script>
@if($errors->all())
	$('#serviceCreateModel').modal('show');
@endif
 $(document).ready(function(){
 	$("#district").change(function() {
	    var filterValue = $(this).val();
	    var options = $('#upazila option');
	    $('#upazila').val('');
	    options.hide()
	    options.each(function(i, el) {
	         if($(el).attr('data-type') == filterValue) {
	             $(el).show();
	         }
	    })
	});
 });

</script>
@endsection
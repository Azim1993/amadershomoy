@extends('layouts.aapp')

@section('title')
	home
@endsection

@section('css')

@endsection

@section('content')
	@include('include.flash')
	<!-- GROUP-LIST -->
	<div class="info-container-top margin-bottom">
		<div class="row">
			<div class="col-sm-8">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
			</div>
			<div class="col-sm-4 text-right">
				<div class="btn-area">
					
					<button data-toggle="modal" data-target="#groupCreateModal" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i> ADD NEW Group</button>
				</div>
			</div>
		</div>
	</div>
	<div id="group-list" class="info-container">
   		<group-index></group-index>

	    <!-- CRT-GRPUPMODAL -->
	    <div class="modal fade {{ $errors->all()?'in':''}}" id="groupCreateModal" style="display: {{ $errors->all()?'block':'none'}}" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog" role="document">
			    <div cl ass="modal-content">
				    <div class="modal-header">
				    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          	<span aria-hidden="true">&times;</span>
				        </button>
				        <h5 class="modal-title" id="groupModal">Create New Group</h5>
				    </div>
				    <div class="modal-body">
				        <form action="{{ route('groups.store') }}" method="POST" enctype="multipart/form-data">
				        	{{csrf_field()}}
				        	<div class="form-group {{ $errors->has('name')?'has-error':'' }}">
					        	<label for="recipient-name" class="form-control-label">Group Name <span>*</span></label>
					        	<input type="text" class="form-control" name="name" placeholder="Group Name">
					        	@include('include.formErrorText', ['inputName' => 'name'])
					        </div>
					        <div class="form-group {{ $errors->has('detail')?'has-error':'' }}">
					        	<label for="recipient-name" class="form-control-label">Summery </label>
					            <textarea class="form-control" id="message-text" name="detail" placeholder="Group Summery"></textarea>
					            @include('include.formErrorText', ['inputName' => 'detail'])
					        </div>
					        <div class="row">
					        	<div class="col-sm-4">
					        		<div class="form-group {{ $errors->has('logoFile')?'has-error':'' }}">
					        			<label for="recipient-name" class="form-control-label">Group Logo</label>
					        			<div class="file_uploader">
					        				<input type="file" name="logoFile" class="form-control">
					        				@include('include.formErrorText', ['inputName' => 'logoFile'])
					        				{{-- <img src="images/grp-thumbnail.png" class="img-responsive" alt="Image Missing"> --}}
					        			</div>
					        		</div>
					        	</div>
					        	<div class="col-sm-8">
					        		<div class="form-group {{ $errors->has('coverFile')?'has-error':'' }}">
					        			<label for="recipient-name" class="form-control-label">Group Cover</label>
					        			<div class="file_uploader">
					        				<input type="file" name="coverFile" class="form-control">
					        				@include('include.formErrorText', ['inputName' => 'coverFile'])
					        				{{-- <img src="images/grp-thumbnail.png" class="img-responsive" alt="Image Missing"> --}}
					        			</div>
					        		</div>
					        	</div>
					        </div>
					        <button type="submit"
					        	class="btn btn-primary modalBtn">
			        			<i class="fa fa-cloud"></i> Store Group
				        	</button>
				        </form>
				    </div>
			    </div>
			</div>
		</div>
	    <!-- /CRT-GRPUPMODAL -->
	</div>
	<!-- /GROUP-LIST -->

@endsection

@section('js')
@if($errors->all())
<script>
 $('#groupCreateModal').modal('show');
</script>
@endif
@endsection
<div class="modal fade" id="blockMessageModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-window-close"></i></button>
				<h5 class="modal-title" id="warningModalLabel">Block Message</h5>
			</div>
			<div class="modal-body">
				<h4 class="text-info">Send Block Message to :<b>
					{{ $user->basicInfo->first_name }}
					{{ $user->basicInfo->last_name }}</b>
				</h4><br>
				<form action="{{ route('admin.user.block.store',[$user->id]) }}" method="post">
					{{ csrf_field() }}
					<div class="form-group {{ $errors->has('block_message')?'has-error':'' }}">
						<textarea name="block_message" class="form-control" placeholder="block message"></textarea>
						@include('include.formErrorText',['inputName' => 'block_message'])
					</div>
					<button type="submit" class="btn btn-danger">
						<i class="fa fa-cloud"></i> Send Message & Block User
					</button>
				</form>
			</div>
		</div>
	</div>
</div>
@extends('layouts.aapp')

@section('title')
	Traning
@endsection

@section('css')

@endsection

@section('content')
	@include('include.flash')
	<!-- GROUP-LIST -->
	<div class="info-container-top margin-bottom">
		<div class="row">
			<div class="col-sm-8">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
			</div>
			<div class="col-sm-4 text-right">
				<div class="btn-area">
					
					<button data-toggle="modal" data-target="#tranignCreateModal" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i> Create Traning</button>
				</div>
			</div>
		</div>
	</div>
	<div id="emergency-service" class="info-container">
	<!-- INFO-TABLE -->
	<div class="info-table table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Logo</th>
					<th>Tranee</th>
					<th>Create</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
	
				
			</tbody>
		</table>
	</div>
	<!-- /INFO-TABLE -->
	<!-- EM-CONTACT MODAL -->
	<div class="modal fade" id="tranignCreateModal" tabindex="-1" role="dialog" aria-labelledby="em-contactLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<h5 class="modal-title" id="warningModalLabel">New Contact</h5>
				</div>
				<div class="modal-body">
					<form action="{{ route('emergency.contact.store') }}" method="post">
						{{ csrf_field() }}
						<div class="row form-group {{ $errors->has('service_id')?'has-error':'' }}">
							<label class="col-sm-4 text-right" for="servie"><b>Service</b></label>
							<div class="col-sm-8">
								<select name="service_id" class="form-control">
									<option value="{{ null }}">Select Service</option>
									@if($services->count() > 0)
										@foreach($services as $service)
											<option value="{{ $service->id }}">{{ $service->name }}</option>
										@endforeach
									@endif
								</select>
							</div>
							@include('include.formErrorText',['inputName' => 'service_id'])
						</div>
						<div class="row form-group {{ $errors->has('district')?'has-error':'' }}">
							<label class="col-sm-4 text-right" for="district"><b>District</b></label>
							<div class="col-sm-8">
								<select id="district" name="district" class="form-control">
									<option value="{{ null }}">Select District</option>
									@if($location->getDistrict()->count() > 0)
										@foreach($location->getDistrict() as $district)
											<option value="{{ $district->id }}">{{ $district->name }}</option>
										@endforeach
									@endif
								</select>
							</div>
							@include('include.formErrorText',['inputName' => 'district'])
						</div>
						<div class="row form-group {{ $errors->has('upazila')?'has-error':'' }}">
							<label class="col-sm-4 text-right" for="upazila"><b>Upazila</b></label>
							<div class="col-sm-8">
								<select id="upazila" name="upazila" class="form-control">
									<option value="{{ null }}">Select Upazila</option>
									@if($location->getUpazila()->count() > 0)
										@foreach($location->getUpazila() as $upazila)
											<option data-type="{{$upazila->district_id}}" value="{{ $upazila->id }}">{{ $upazila->name }}</option>
										@endforeach
									@endif
								</select>
							</div>
							@include('include.formErrorText',['inputName' => 'upazila'])
						</div>
						<div class="row form-group {{ $errors->has('location')?'has-error':'' }}">
							<label class="col-sm-4 text-right" for="location"><b>Location</b></label>
							<div class="col-sm-8">
								<textarea name="location" class="form-control" placeholder="Enter Location"></textarea> 
							</div>
							@include('include.formErrorText',['inputName' => 'location'])
						</div>
						<div class="row form-group {{ $errors->has('contact_number')?'has-error':'' }}">
							<label class="col-sm-4 text-right" for="contact_number"><b>Contact Number</b></label>
							<div class="col-sm-8">
								<input name="contact_number" class="form-control" placeholder="Enter Location">
							</div>
							@include('include.formErrorText',['inputName' => 'contact_number'])
						</div>
						<div class="row form-group">
							<div class="col-sm-4 text-right"></div>
							<div class="col-sm-8">
								<button type="submit" class="btn btn-sm btn-primary">
									<i class="fa fa-cloud"></i> Upload Contact
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>

@endsection
@section('js')
<script>
@if($errors->all())
	$('#serviceCreateModel').modal('show');
@endif
</script>
@endsection
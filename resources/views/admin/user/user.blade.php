@extends('layouts.aapp')

@section('title')
	Traning
@endsection

@section('css')

@endsection

@section('content')
	@include('include.flash')
	<!-- GROUP-LIST -->
	<div class="info-container-top margin-bottom">
		<div class="row">
			<div class="col-sm-8">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
			</div>
		</div>
	</div>
	<div id="emergency-service" class="info-container">
	<!-- INFO-TABLE -->
	<div class="info-table table-responsive">
		@if($users->count() > 0)
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>desingnation</th>
					<th>userName</th>
					<th>location</th>
					<th>contact</th>
					<th>Join Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $key => $user)
				<tr>
					<td>{{$key + 1}}</td>
					<td>{{$user->basicInfo->first_name.' '.$user->basicInfo->last_name}}</td>
					<td>{{ $user->basicInfo->profession }}</td>
					<td>{{ $user->user_name?:'_' }}</td>
					<td>{{ $user->basicInfo->address }} <br>
						{{ $user->basicInfo->district }}</td>
					<td>{{ $user->phone }} <br>
						{{ $user->email }}</td>
					<td>{{ Carbon\Carbon::parse($user->created_at)->toFormattedDateString()  }}</td>
					<td>
						<a href="#" data-toggle="modal" data-target="#warnignMessageModal" class="btn btn-xs btn-warning">Warning</a>
						<a href="#" data-toggle="modal" data-target="#blockMessageModal" class="btn btn-xs btn-danger">Block</a>
					</td>
				</tr>
				@include('admin.user.modal.warning',['user' => $user])
				@include('admin.user.modal.block',['user' => $user])
				@endforeach
			</tbody>
		</table>
		@else
		<div class="alert alert-warning">No user</div>
		@endif
	</div>
	<!-- /INFO-TABLE -->
	</div>

@endsection
@section('js')
<script>
@if($errors->all())
	$('#serviceCreateModel').modal('show');
@endif
</script>
@endsection
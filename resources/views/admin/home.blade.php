@extends('layouts.aapp')

@section('title')
	home
@endsection

@section('css')

@endsection

@section('content')
<div id="user-list" class="info-container">
    <!-- INFO-TABLE -->
   	<div class="info-table table-responsive">
   		<table id="example" class="table table-striped display" cellspacing="0" width="100%">
		    <thead>
			    <tr>
			        <th class="all">
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</th>
			        <th>#</th>
			        <th>Name</th>
			        <th>Designation</th>
			        <th>Location</th>
			        <th>Contact</th>
			        <th>Join Date</th>
			        <th>Action</th>
			    </tr>
		    </thead>
		    <tfoot>
	            <tr>
	                <th class="footer-th-hidden">Name</th>
	                <th class="footer-th-hidden">Position</th>
	                <th class="footer-th-hidden">Office</th>
	                <th>Designation</th>
	                <th>Location</th>
	                <th class="footer-th-hidden">Salary</th>
	                <th class="footer-th-hidden">Start date</th>
	                <th class="footer-th-hidden">Salary</th>
	            </tr>
	        </tfoot>
		    <tbody>
		    	<tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Dhanmondi, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Fahad</td>
			        <td>Designer</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
			    <tr>
			        <td>
			        	<div class="checkbox">
					        <label>
					          <input type="checkbox">
					        </label>
				      	</div>
						</td>
			        <td>001</td>
			        <td>Elias</td>
			        <td>Journalist</td>
			        <td>Banani, Dhaka</td>
			        <td>+88 12345678910<br>john@example.com</td>
			        <td>5-Sep-2017</td>
			        <td><span data-toggle="modal" data-target="#warningModal" class="warning">Warning</span><span class="block">Block</span><span class="edit">Edit</span><span class="del">Delete</span></td>
			    </tr>
		    </tbody>
		</table>
   	</div>
    <!-- /INFO-TABLE -->

    <!-- WARNING-MESSAGE MODAL -->
    <div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="warningModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
		    <div class="modal-content">
			    <div class="modal-header">
			    	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          	<span aria-hidden="true">&times;</span>
			        </button>
			        <h5 class="modal-title" id="warningModalLabel">Sent Warning Message</h5>
			    </div>
			    <div class="modal-body">
			        <form>
				        <div class="form-group">
				            <textarea class="form-control" id="message-text" rows="7" placeholder="Type message"></textarea>
				        </div>
			        </form>
			    </div>
			    <div class="modal-footer">
			        <button type="button" class="btn btn-primary">Send message</button>
			    </div>
		    </div>
		</div>
	</div>
    <!-- /WARNING-MESSAGE MODAL -->
</div>
<!--==================================
/CONTENT
===================================-->

@endsection

@section('js')

@endsection
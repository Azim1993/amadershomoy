@extends('layouts.aapp')

@section('title')
	Traning
@endsection

@section('css')

@endsection

@section('content')
	@include('include.flash')
	<!-- GROUP-LIST -->
	<div class="info-container-top margin-bottom">
		<div class="row">
			<div class="col-sm-8">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
			</div>
			<div class="col-sm-4 text-right">
				<div class="btn-area">
					
					<button data-toggle="modal" data-target="#tranignCreateModal" class="btn btn-sm btn-primary">
						<i class="fa fa-plus" aria-hidden="true"></i> Create Traning</button>
				</div>
			</div>
		</div>
	</div>
	<div id="emergency-service" class="info-container">
	<!-- INFO-TABLE -->
	<div class="info-table table-responsive">
		@if($trainings->count() > 0)
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Logo</th>
					<th>Trainee</th>
					<th>Create</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($trainings as $key=>$training)
				<tr>
					<td>{{ $key+1 }}</td>
					<td>{{ $training->name }}</td>
					<td><img src="/storage/{{ $training->logo }}" width="40" alt="{{ $training->name }}"></td>
					<td>{{ $training->users->count() }}</td>
					<td>{{ Carbon\Carbon::parse($training->created_at)->toFormattedDateString() }}</td>
					<td>
						<a href="{{ route('members.index',$training) }}" class="btn btn-primary btn-xs">Members</a>
						<a href="#" class="btn btn-success btn-xs">Edit</a>
						<a href="#" class="btn btn-danger btn-xs">
							<i class="fa fa-window-close"></i>
						</a>
					</td>
				</tr>
				@endforeach	
			</tbody>
		</table>
		@else
			<div class="alert alert-info">No Training</div>
		@endif
	</div>
	<!-- /INFO-TABLE -->
	<!-- EM-CONTACT MODAL -->
	<div class="modal fade" id="tranignCreateModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
					</button>
					<h5 class="modal-title" id="warningModalLabel">New Training</h5>
				</div>
				<div class="modal-body">
					<form enctype="multipart/form-data" action="{{ route('training.store') }}" method="post">
						{{ csrf_field() }}
						<div class="row form-group {{ $errors->has('name')?'has-error':'' }}">
							<label class="col-sm-4 text-right" for="servie"><b>Training name</b></label>
							<div class="col-sm-8">
								<input type="text" class="form-control" placeholder="training Nmae" name="name">
								@include('include.formErrorText',['inputName' => 'name'])
							</div>
						</div>
						<div class="row form-group {{ $errors->has('logo')?'has-error':'' }}">
							<label class="col-sm-4 text-right" for="servie"><b>Training Logo</b></label>
							<div class="col-sm-8">
								<input type="file" class="form-control" placeholder="training Nmae" name="logo">
								@include('include.formErrorText',['inputName' => 'logo'])
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-4 text-right"></div>
							<div class="col-sm-8">
								<button type="submit" class="btn btn-sm btn-primary">
									<i class="fa fa-cloud"></i> Store Training
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>

@endsection
@section('js')
<script>
@if($errors->all())
	$('#tranignCreateModal').modal('show');
@endif
</script>
@endsection
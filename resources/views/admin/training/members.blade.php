@extends('layouts.aapp')

@section('title')
	Traning
@endsection

@section('css')
<style>
	.btn-area {
		position: relative;
		margin-top: -15px;
		padding-bottom: 15px;
	}
	.memberAddInput{
		width: 82%;
	    border-radius: 0;
	}
	.memberAddBtn {
		position: absolute;
	    right: 0px;
	    top: 0px;
	    padding: 7px 21px;
	    border-radius: 0;
	}
</style>
@endsection

@section('content')
	@include('include.flash')
	<!-- GROUP-LIST -->
	<div class="info-container-top margin-bottom">
		<div class="row">
			<div class="col-sm-8">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
			</div>
		</div>
	</div>
	<div id="emergency-service" class="info-container">

	<div class="training_hint">
		<h4 class="text-info">Training : <b>{{ $training->name }}</b></h4><br>
	</div>
	<div class="row">
		<div class="col-sm-4"><b>Training Users</b></div>
		<div class="col-sm-8 text-right">
			<admin-user-search></admin-user-search>
		</div>
	</div><hr>
	<!-- INFO-TABLE -->
	<div class="info-table table-responsive">
		@if($training->users->count() > 0)
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Email</th>
					<th>UserName</th>
					<th>phone</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($training->users as $key=>$member)
				<tr>
					<td>{{ $key+1 }}</td>
					<td>{{ $member->basicInfo->first_name.' '.$member->basicInfo->last_name }}</td>
					<td>{{ $member->email }}</td>
					<td>{{ $member->user_name?:'-' }}</td>
					<td>{{ $member->phone?:'-' }}</td>
					<td>
						<a href="#" class="btn btn-success btn-xs">Edit</a>
						<a href="#" class="btn btn-danger btn-xs">
							<i class="fa fa-window-close"></i>
						</a>
					</td>
				</tr>
				@endforeach	
			</tbody>
		</table>
		@else
			<div class="alert alert-info">No Training</div>
		@endif
	</div>
	</div>

@endsection
@section('js')
@endsection
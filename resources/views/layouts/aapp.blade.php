<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'amaderShomoy') }} | @yield('title')</title>
	
	<!--required css ---->
	@include('layouts.admin.css')
    <!-- Styles -->
    <!-- yield css -->
    @yield('css')
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar-fixed-top" data-offset="60">
	<div id="main">
		@include('layouts.admin.header')
        <section id="app">
            <div class="container-fluid">
                @if(Auth::check())
                    <div class="row">
                        @include('layouts.admin.leftMenubar')
                        <div class="col-md-11 grid-col-3">
                            @yield('content')
                        </div>
                    </div>
                @else
                    @yield('content')
                @endif
            </div>
        </section>
		@include('layouts.admin.footer')
		@yield('js')
	</div>
</body>
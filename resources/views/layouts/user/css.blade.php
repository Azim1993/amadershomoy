<link rel="stylesheet" href="{{ asset('/plugins/bootstrap/bootstrap.css') }}">
<link href="{{ asset('/plugins/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css')}}"/>
<link href="{{ asset('/plugins/select2-master/dist/css/select2.min.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('/plugins/owl_carosel/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('/plugins/owl_carosel/owl.theme.default.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('/plugins/simple_lightbox/simpleLightbox.css') }}">
<link rel="stylesheet" href="{{ asset('/css/font-awesome.css') }}">		<!-- Font Awesome stylesheet-->
<link rel="stylesheet" href="{{ asset('/css/style.css') }}">
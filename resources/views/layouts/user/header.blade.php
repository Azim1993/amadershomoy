<a id="scroll-top" href="#top">
	<button><img src="{{ asset('/images/arow-up.png') }}"/></button>
</a>
<header class="main-header-area overflow-fix">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<nav class="client-menu-right overflow-fix">
					<ul class="">
						<li>
							<a href="{{ url('/') }}" class="active">
								<span><i class="fa fa-home" aria-hidden="true"></i></span>
								Home
							</a>
						</li>
						<li>
							<a href="{{ route('user.profile.network') }}">
								<span><i class="fa fa-user" aria-hidden="true"></i></span>
								Network
							</a>
						</li>
						<li>
							<a href="{{ route('user.groups') }}">
								<span><i class="fa fa-bullhorn" aria-hidden="true"></i></span>
								Groups
							</a>
						</li>
						
						
						<li class="overflow-icon">
							<a href="#"   id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span><i class="fa fa-comments" aria-hidden="true"></i></span>
								<span>1</span>
								Messages
							</a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<div class="single-modal-nofi box-white-bg overflow-fix">
										<a href="">
											<img src="images/profile.png"/>
											<p><span>Toggle a working</span> modal demo by clicking the button below. It will slide down and fade</p>
											<h6>01:00</h6>
										</a>
									</div>
									<div class="single-modal-nofi box-white-bg overflow-fix">
										<a href="">
											<img src="images/profile.png"/>
											<p><span>Toggle a working</span> modal demo by clicking the button below. It will slide down and fade</p>
											<h6>Thu</h6>
										</a>
									</div>
									<div class="single-modal-nofi box-white-bg overflow-fix">
										<a href="">
											<img src="images/profile.png"/>
											<p><span>Toggle a working</span> modal demo by clicking the button below. It will slide down and fade</p>
											<h6>01:00</h6>
										</a>
									</div>
									<div class="single-modal-nofi box-white-bg overflow-fix">
										<a href="">
											<img src="images/profile.png"/>
											<p><span>Toggle a working</span> modal demo by clicking the button below. It will slide down and fade</p>
											<h6>Thu</h6>
										</a>
									</div>
									<div class="seel-abuton-menu overflow-fix">
										<a href="">See All</a>
									</div>
							</div>
						</li>
						
						
						<li class="overflow-icon">
							<a href="#"   id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span><i class="fa fa-bell-o" aria-hidden="true"></i></span>
								<span>1</span>
								Notification
							</a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<div class="single-modal-nofi box-white-bg overflow-fix">
										<a href="">
											<img src="images/profile.png"/>
											<p><span>Toggle a working</span> modal demo by clicking the button below. It will slide down and fade</p>
										</a>
									</div>
									<div class="single-modal-nofi box-white-bg overflow-fix">
										<a href="">
											<img src="images/profile.png"/>
											<p><span>Toggle a working</span> modal demo by clicking the button below. It will slide down and fade</p>
										</a>
									</div>
									<div class="single-modal-nofi box-white-bg overflow-fix">
										<a href="">
											<img src="images/profile.png"/>
											<p><span>Toggle a working</span> modal demo by clicking the button below. It will slide down and fade</p>
										</a>
									</div>
									<div class="single-modal-nofi box-white-bg overflow-fix">
										<a href="">
											<img src="images/profile.png"/>
											<p><span>Toggle a working</span> modal demo by clicking the button below. It will slide down and fade</p>
										</a>
									</div>
									<div class="seel-abuton-menu overflow-fix">
										<a href="">See All</a>
									</div>
							</div>
						</li>
						
					</ul>
				</nav>                    
			</div>
			<div class="col-lg-6">
					<nav class="client-menu-right overflow-fix header-menu-right d-flex justify-content-end">
							<div class="group-mamber-add-search">
								<i class="fa fa-search" aria-hidden="true"></i>
								<input type="search" placeholder="Search" name="email">
							</div>
							<ul class="d-flex justify-content-end">
								<li class="overflow-icon">
									<a href="#" @click="visible = !visible" class="avaterBtn">
										<div class="profile-pic-right">
											<img src="{{ asset('/images/profile.png') }}">
										</div>
									</a>
									<div v-if="visible" class="avaterDropdown">
										<div class="single-modal-nofi box-white-bg overflow-fix">
											<a href="{{ route('user.profile')}}">Profile</a>
										</div>
										<div class="single-modal-nofi box-white-bg overflow-fix">
											<a href="{{ route('user.profile.update')}}">Setting</a>
										</div>
										<div class="single-modal-nofi box-white-bg overflow-fix">
											@include('include.logout')
										</div>
										
									</div>
								</li>
							</ul>
						</nav>  
				</div>   
		</div>
	</div>
</header>
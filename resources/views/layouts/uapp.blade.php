<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'amaderShomoy') }} | @yield('title')</title>
	
	<!--required css ---->
	 @include('layouts.user.css')
    <!-- Styles -->
    <!-- yield css -->
    @yield('css')
</head>
<body>
	<div id="app">
		@include('layouts.user.header')
		@yield('content')
	</div>
    @include('layouts.user.footer')
    @yield('js')
</body>
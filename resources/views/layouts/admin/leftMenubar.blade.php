    <!-- GRID-COL-1 -->
    <div class="col-md-1 grid-col-1">
        <div class="d-option">
            <ul class="d-option-list">
                <li class="user">
                    <a href="#">
                    <img class="img-block" src="/admin/images/ic-2.png" alt="Icon Missing">
                    <img class="img-hover" src="/admin/images/ic-hover-2.png" alt="Icon Missing">
                    <br>Users
                    </a>
                    <ul class="sub-menu-list">
                        <li>
                            <a href="{{ route('admin.user.all') }}" id="userList" class="sub-active">&nbsp;User List</a>
                        </li>
                        <li>
                            <a href="block_users.html" id="blockUserList">&nbsp;Block User List</a>
                        </li>
                    </ul>
                </li>
                <li class="em-service">
                    <a href="#">
                    <img class="img-block" src="/admin/images/ic-3.png" alt="Icon Missing">
                    <img class="img-hover" src="/admin/images/ic-hover-3.png" alt="Icon Missing">
                    <br>Emergency
                    </a>
                    <ul class="sub-menu-list">
                        <li>
                            <a href="{{ route('emergency-service.index') }}" id="emergencyContact">&nbsp;Emergency Contact</a>
                        </li>
                    </ul>
                </li>
                <li class="training">
                    <a href="#">
                    <img class="img-block" src="/admin/images/ic-4.png" alt="Icon Missing">
                    <img class="img-hover" src="/admin/images/ic-hover-4.png" alt="Icon Missing">
                    <br>Training
                    </a>
                    <ul class="sub-menu-list">
                        <li>
                            <a href="{{ route('training.index') }}" id="videoList">&nbsp;Training List</a>
                        </li>
                        <li>
                            <a href="video_list.html" id="videoList">&nbsp;Video List</a>
                        </li>
                        <li>
                            <a href="audio_list.html" id="audioList">&nbsp;Audio List</a>
                        </li>
                    </ul>
                </li>
                <li class="online-poll">
                    <a href="#">
                    <img class="img-block" src="/admin/images/ic-5.png" alt="Icon Missing">
                    <img class="img-hover" src="/admin/images/ic-hover-5.png" alt="Icon Missing">
                    <br>Online Poll
                    </a>
                    <ul class="sub-menu-list">
                        <li>
                            <a href="poll_list.html" id="pollList">&nbsp;Poll List</a>
                        </li>
                    </ul>
                </li>
                <li class="group">
                    <a href="#">
                    <img class="img-block" src="/admin/images/ic-6.png" alt="Icon Missing">
                    <img class="img-hover" src="/admin/images/ic-hover-6.png" alt="Icon Missing">
                    <br>Group
                    </a>
                    <ul class="sub-menu-list">
                        <li>
                            <a href="{{ route('groups.index') }}" id="groupList">&nbsp;Group List</a>
                        </li>
                        <li>
                            <a href="{{ route('sub_groups.index')}}" id="subGroupList">&nbsp;Sub Group List</a>
                        </li>
                    </ul>
                </li>
                <li class="advertise">
                    <a href="#">
                    <img class="img-block" src="/admin/images/ic-7.png" alt="Icon Missing">
                    <img class="img-hover" src="/admin/images/ic-hover-7.png" alt="Icon Missing">
                    <br>Advertise
                    </a>
                    <ul class="sub-menu-list">
                        <li>
                            <a href="add_list.html" id="advertiseList">&nbsp;Add List</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
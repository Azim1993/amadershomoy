<!--==================================
HEADER
===================================-->
<header id="header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-offset-1 col-sm-3 logo">
				<a href="#" class="navbar-brand">
					<img src="/admin/images/logo.png" alt="Logo Missing">
				</a>
			</div>
			@if(Auth::check())
			<div class="col-sm-offset-3 col-sm-5">
				<nav class="navbar">
					<ul id="amder-somoy-dashboard" class="nav navbar-nav navbar-right">
					    <li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-bell fa-2x" aria-hidden="true"></i></a>
							<ul class="amder-somoy-dashboard dropdown-menu">
							   	<li><a href="">If you are going to use a passage of Lorem Ipsum</a></li>	<li><a href="">If you are going to use a passage of Lorem Ipsum</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="/admin/images/man.png" alt="Icon Missing"></a>
							<ul class="amder-somoy-dashboard dropdown-menu">
							   	<li><a href="#">Profile Settings</a></li>
							   	<li>@include('include.logout')</li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
			@endif
		</div>
	</div>
</header>
<!--==================================
/HEADER
===================================-->
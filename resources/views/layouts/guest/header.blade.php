<section class="sing-up-area overflow-fix">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo-s overflow-fix">
                        <a href="{{ route('welcome') }}"><img src="{{ asset('/images/guest/logo-s.png') }}"/></a>
                    </div>
                </div>
                <div class="col-md-9">
                    <form action="{{ route('login') }}"  method="POST" class="singe-up-form-area overflow-fix d-flex justify-content-end">
                        {{csrf_field()}}
                        <div class="singe-up-form-single-area overflow-fix">
                            <input type="text" name="email" placeholder="Email"/>
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </div>
                        <div class="singe-up-form-single-area overflow-fix">
                            <input type="password" name="password" placeholder="Password"/>
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </div>
                        <button type="submit">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </section>